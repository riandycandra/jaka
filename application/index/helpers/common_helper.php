<?php

function terbilang($nilai) {
	$nilai = abs($nilai);
	$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " ". $huruf[$nilai];
	} else if ($nilai <20) {
		$temp = terbilang($nilai - 10). " belas";
	} else if ($nilai < 100) {
		$temp = terbilang($nilai/10)." puluh". terbilang($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " seratus" . terbilang($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = terbilang($nilai/100) . " ratus" . terbilang($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " seribu" . terbilang($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = terbilang($nilai/1000) . " ribu" . terbilang($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = terbilang($nilai/1000000) . " juta" . terbilang($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = terbilang($nilai/1000000000) . " milyar" . terbilang(fmod($nilai,1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = terbilang($nilai/1000000000000) . " trilyun" . terbilang(fmod($nilai,1000000000000));
	}     
	return $temp;
}

function subword($string)
{
	$s = substr($string, 0, 150);
   $result = substr($s, 0, strrpos($s, ' '));
   return $result;
}

function running_text()
{
	$CI =& get_instance();
	$news = $CI->db->select('n.id, n.title, n.content')->join('category c', 'c.id = n.category_id', 'INNER')->order_by('n.created_on DESC')->get('news n', 0, 5)->result();

	$text = '';
	foreach($news as $row) :
		$text .= strip_tags($row->content, 'p');
	endforeach;

	return $text;
}

function today(){
	$hari = date ("D");
 
	switch($hari){
		case 'Sun':
			$hari_ini = "Minggu";
		break;
 
		case 'Mon':			
			$hari_ini = "Senin";
		break;
 
		case 'Tue':
			$hari_ini = "Selasa";
		break;
 
		case 'Wed':
			$hari_ini = "Rabu";
		break;
 
		case 'Thu':
			$hari_ini = "Kamis";
		break;
 
		case 'Fri':
			$hari_ini = "Jumat";
		break;
 
		case 'Sat':
			$hari_ini = "Sabtu";
		break;
		
		default:
			$hari_ini = "Tidak di ketahui";		
		break;
	}
 
	return $hari_ini;
 
}

function tgl_indo($tanggal)
{
	$bulan = array (
		1 =>   'Januari',
		2 =>'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function rupiah($angka)
{
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}

function debug($str)
{
	echo '<pre>';
	print_r($str);
	die();
}