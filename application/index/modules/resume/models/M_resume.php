<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_resume extends CI_Model {

	public function get_list($matakuliah = null)
	{
		return $this->db->query("SELECT `matakuliah`.`nama` , `resume`.`pertemuan_no` , `resume`.`content` , `resume`.`id` FROM `penjadwalan_genetik_web`.`resume` INNER JOIN `penjadwalan_genetik_web`.`pengampu` ON(`resume`.`pengampu_id` = `pengampu`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`dosen` ON (`resume`.`user_id` = `dosen`.`kode`) AND (`pengampu`.`kode_dosen` = `dosen`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`matakuliah` ON (`pengampu`.`kode_mk` = `matakuliah`.`kode`) WHERE matakuliah.kode = '$matakuliah' ORDER BY pertemuan_no ASC")->result();
	}

	public function get_detail($id = null)
	{
		return $this->db->query("SELECT `matakuliah`.`nama` , `resume`.`pertemuan_no` , `resume`.`content` , `resume`.`id` FROM `penjadwalan_genetik_web`.`resume` INNER JOIN `penjadwalan_genetik_web`.`pengampu` ON(`resume`.`pengampu_id` = `pengampu`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`dosen` ON (`resume`.`user_id` = `dosen`.`kode`) AND (`pengampu`.`kode_dosen` = `dosen`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`matakuliah` ON (`pengampu`.`kode_mk` = `matakuliah`.`kode`) WHERE resume.id = '$id' ORDER BY pertemuan_no ASC")->row();
	}

}