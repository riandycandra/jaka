<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/head-panel') ?>

  <div class="row row-sm mg-t-20">
    <div class="col-12">
      <div class="text-center"><h2>RINGKASAN MATERI </h2></div>
    </div>
  </div>

  <div class="row row-sm mg-t-10">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12 mg-l-20">
          <form action="" class="form-horizontal">
            <div class="form-group">
              <label for="" class="control-label col-md-2 tx-30">Mata Kuliah</label>
              <div class="col-md-10">
                <span class="tx-30"><?php echo $data->nama ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="control-label col-md-2 tx-30">Pertemuan Ke</label>
              <div class="col-md-10">
                <div class="tx-30"><?php echo terbilang($data->pertemuan_no) ?></div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="row row-sm mg-t-10">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <h2>Ringkasan Materi <b><?php echo $data->nama ?></b> Pertemuan Ke <?php echo $data->pertemuan_no ?></h2>
          <h3 class="mg-t-20 text-justify"><?php echo $data->content ?></h3>
        </div>
      </div>
    </div>
  </div>

  <div class="row mg-t-70" style="position:fixed; bottom:5px;">
    <div class="col-2">
      <div class="card card-body pd-x-10 pd-y-0">
        <h1 class="card-text text-right"><a href="#">News</a></h1>
      </div>
    </div>
    <div class="col-10">
      <div class="card card-body pd-x-0 pd-y-0">
        <h1 class="card-text"><marquee behavior="" direction="left" scrollamount="12"><?php echo running_text() ?></marquee></h1>
      </div>
    </div>
  </div>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<?php $this->load->view('template/scripts') ?>
<script>
  setInterval(function() {
    if($('blink').css('opacity') == "0") {
      $('blink').css('opacity', 1);
    } else {
      $('blink').css('opacity', 0);
    }
  }, 400);
</script>
</body>
</html>
