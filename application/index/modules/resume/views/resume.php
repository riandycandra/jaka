<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/head-panel') ?>

  <div class="row row-sm mg-t-20">
    <div class="col-12">
      <div class="text-center"><h2>RINGKASAN MATERI </h2></div>
    </div>
  </div>

  <div class="row row-sm mg-t-10">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-4 mg-l-20">
          <form action="#" method="post" class="form-inline">
            <div class="form-group">
              <select name="matakuliah" id="" class="form-control select2-show-search" data-placeholder="Choose one (with searchbox)">
                <?php echo $dropdown_matakuliah ?>
              </select>
            </div>
            <input type="submit" name="submit" value="Submit" class="btn btn-default mg-l-20">
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="row row-sm mg-t-10">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th class="text-center">MATA KULIAH</th>
                    <th class="text-center">PERTEMUAN KE</th>
                    <th class="text-center">ISI</th>
                    <th>#</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($data as $row) : ?>
                  <tr>
                    <td class="pt-3-half" ><?php echo $row->nama ?></td>
                    <td class="pt-3-half text-center" ><?php echo $row->pertemuan_no ?></td>
                    <td class="pt-3-half" ><?php echo subword(strip_tags($row->content, 'p')) ?></td>
                    <td class="pt-3-half"><a href="<?php echo base_url('resume/detail/'.$row->id) ?>" class="btn btn-secondary btn-md"><i class="fa fa-eye"></i> View</a></td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row mg-t-70" style="position:fixed; bottom:5px;">
    <div class="col-2">
      <div class="card card-body pd-x-10 pd-y-0">
        <h1 class="card-text text-right"><a href="#">News</a></h1>
      </div>
    </div>
    <div class="col-10">
      <div class="card card-body pd-x-0 pd-y-0">
        <h1 class="card-text"><marquee behavior="" direction="left" scrollamount="12"><?php echo running_text() ?></marquee></h1>
      </div>
    </div>
  </div>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<?php $this->load->view('template/scripts') ?>
<script>
  setInterval(function() {
    if($('blink').css('opacity') == "0") {
      $('blink').css('opacity', 1);
    } else {
      $('blink').css('opacity', 0);
    }
  }, 400);
</script>
</body>
</html>
