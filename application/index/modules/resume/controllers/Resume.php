<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resume extends MY_Controller {
	public function __construct()
	{
		$this->load->model('M_resume', 'resume');
	}

	public function index()
	{
		$data['title'] = "Resume";
		$data['dropdown_matakuliah'] = $this->dropdown_matakuliah();
		
		if($this->input->post('submit'))
		{
			$data['data'] = $this->resume->get_list($this->input->post('matakuliah'));
		} else {
			$data['data'] = array();
		}

		$this->load->view('resume', $data);
	}

	public function detail($id = null)
	{
		$data['title'] = "Resume";
		$data['data'] = $this->resume->get_detail($id);
		
		if(!isset($data['data']->nama))
		{
			redirect(base_url());
		}
		// debug($data);
		$this->load->view('detail', $data);
	}

	public function dropdown_matakuliah($selected = null)
	{
		$option = '';
		$data = $this->crud->get_all('matakuliah','*', 'nama ASC');
		if($data){
			foreach($data as $row){
				$select = ($selected == $row->kode ? 'selected' : '');
				$option .= '<option value="' . $row->kode . '" '.$select.'>' . $row->nama . '</option>';
			}
		}
		return $option;
	}

}