<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends MY_Controller {
	public function __construct()
	{
		$this->load->model('M_jadwal', 'jadwal');
	}

	public function index()
	{
		if($this->input->post('submit'))
		{
			$semester = $this->input->post('semester');
		} else {
			$semester = null;
		}
		$hari = today();
		$data['data'] = $this->jadwal->get_jadwal($hari, $semester);
		$data['title'] = "Jadwal";
		$this->load->view('jadwal', $data);
	}

}