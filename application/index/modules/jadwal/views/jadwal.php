<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/head-panel') ?>

  <div class="row row-sm mg-t-20">
    <div class="col-12">
      <div class="text-center"><h2>JADWAL MATA KULIAH</h2></div>
    </div>
  </div>

  <div class="row row-sm mg-t-10">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-4 mg-l-20">
          <form action="<?php echo base_url('jadwal') ?>" method="post" class="form-inline">
            <div class="form-group">
              <select name="semester" id="" class="form-control select2-show-search" data-placeholder="Choose one (with searchbox)">
                  <option value="1">SEMESTER 1</option>
                  <option value="2">SEMESTER 2</option>
                  <option value="3">SEMESTER 3</option>
                  <option value="4">SEMESTER 4</option>
                  <option value="5">SEMESTER 5</option>
                  <option value="6">SEMESTER 6</option>
                  <option value="7">SEMESTER 7</option>
                  <option value="8">SEMESTER 8</option>
                </select>
            </div>
            <input type="submit" name="submit" value="Submit" class="btn btn-default mg-l-20">
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="row row-sm mg-t-10">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th>HARI</th>
                    <th>JAM</th>
                    <th>MATA KULIAH</th>
                    <th>SKS</th>
                    <th>SEMESTER</th>
                    <th>DOSEN</th>
                    <th>RUANG</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($data as $row) : ?>
                  <tr>
                    <td><?php echo $row->hari ?></td>
                    <td><?php echo $row->jam ?></td>
                    <td><?php echo $row->matakuliah ?></td>
                    <td><?php echo $row->sks ?></td>
                    <td><?php echo $row->semester ?></td>
                    <td><?php echo $row->dosen ?></td>
                    <td><?php echo $row->ruang ?></td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row mg-t-70" style="position:fixed; bottom:5px;">
    <div class="col-2">
      <div class="card card-body pd-x-10 pd-y-0">
        <h1 class="card-text text-right"><a href="#">News</a></h1>
      </div>
    </div>
    <div class="col-10">
      <div class="card card-body pd-x-0 pd-y-0">
        <h1 class="card-text"><marquee behavior="" direction="left" scrollamount="12"><?php echo running_text() ?></marquee></h1>
      </div>
    </div>
  </div>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<?php $this->load->view('template/scripts') ?>
<script>
  setInterval(function() {
    if($('blink').css('opacity') == "0") {
      $('blink').css('opacity', 1);
    } else {
      $('blink').css('opacity', 0);
    }
  }, 400);
</script>
</body>
</html>
