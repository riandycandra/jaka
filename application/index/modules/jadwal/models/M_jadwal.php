<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jadwal extends CI_Model {

	public function get_jadwal($hari = null, $semester = null)
	{
		if($semester != null)
		{
			$semester = "AND matakuliah.semester = '$semester'";
		}
		return $this->db->query("SELECT `hari`.`nama` AS hari , `ruang`.`nama` AS ruang , `jam`.`range_jam` AS jam , `matakuliah`.`nama` AS matakuliah, `matakuliah`.`sks` as sks, `matakuliah`.`semester` as semester, `dosen`.`nama` as dosen FROM `penjadwalan_genetik_web`.`pengampu` INNER JOIN `penjadwalan_genetik_web`.`dosen` ON(`pengampu`.`kode_dosen` = `dosen`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`jadwalkuliah` ON (`jadwalkuliah`.`kode_pengampu` = `pengampu`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`ruang` ON (`jadwalkuliah`.`kode_ruang` = `ruang`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`jam` ON (`jadwalkuliah`.`kode_jam` = `jam`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`hari` ON (`jadwalkuliah`.`kode_hari` = `hari`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`matakuliah` ON (`pengampu`.`kode_mk` = `matakuliah`.`kode`) WHERE hari.nama = '$hari' $semester ORDER BY range_jam ASC")->result();
	}

}