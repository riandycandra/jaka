<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/head-panel') ?>

  <div class="row row-sm mg-t-20">
    <div class="col-12">
      <div class="text-center"><h2>PENGUMUMAN</h2></div>
    </div>
  </div>

  <div class="row row-sm-mg-t-10">
    <div class="col-12">
      <?php foreach($data as $row) : ?>
      <div class="row mg-b-10">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h3 class="card-title tx-dark tx-medium mg-b-10 text-center"><?php echo $row->author ?></h3>
              <h2 class="card-subtitle tx-normal mg-b-15"><u><?php echo $row->title ?></u></h2>
              <h4 class="card-text"><?php echo strip_tags($row->content, 'p') ?></h4>
            </div>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>

  <div class="row mg-t-70" style="position:fixed; bottom:5px;">
    <div class="col-2">
      <div class="card card-body pd-x-10 pd-y-0">
        <h1 class="card-text text-right"><a href="#">News</a></h1>
      </div>
    </div>
    <div class="col-10">
      <div class="card card-body pd-x-0 pd-y-0">
        <h1 class="card-text"><marquee behavior="" direction="left" scrollamount="12"><?php echo running_text() ?></marquee></h1>
      </div>
    </div>
  </div>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<?php $this->load->view('template/scripts') ?>
<script>
  setInterval(function() {
    if($('blink').css('opacity') == "0") {
      $('blink').css('opacity', 1);
    } else {
      $('blink').css('opacity', 0);
    }
  }, 400);
</script>
</body>
</html>
