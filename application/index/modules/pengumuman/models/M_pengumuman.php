<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengumuman extends CI_Model {

	public function get_all()
	{
		$now = date('Y-m-d');
		$this->db->select('p.id, p.title, p.content, u.name as author');
		$this->db->join('user u', 'p.user_id = u.id', 'INNER');
		$this->db->where(array('p.status' => '2', 'expired_on >=' => $now));
		$this->db->order_by('created_on DESC');
		return $this->db->get('pengumuman p', 0, 5)->result();
	}

	public function get_detail($id = null)
	{
		$this->db->select('p.id, p.title, p.content, u.name as author');
		$this->db->join('user u', 'p.user_id = u.id', 'INNER');
		$this->db->where(array('p.status' => '2', 'p.id' => $id));
		$this->db->order_by('created_on DESC');
		return $this->db->get('pengumuman p', 0, 5)->result();
	}

}