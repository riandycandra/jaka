<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends MY_Controller {
	public function __construct()
	{
		$this->load->model('M_pengumuman', 'pengumuman');
	}

	public function index()
	{
		$data['data'] = $this->pengumuman->get_all();
		$data['title'] = "Pengumuman";
		$this->load->view('pengumuman', $data);
	}

	public function detail($id = null)
	{
		$data['data'] = $this->pengumuman->get_detail($id);
		$data['title'] = "Pengumuman";
		$this->load->view('pengumuman_detail', $data);
	}

}