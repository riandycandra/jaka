<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/head-panel') ?>

  <div class="row row-sm mg-t-20 mg-b-20">
    <div class="col-12">
      <div class="text-center"><h2>EVENT</h2></div>
    </div>
  </div>

  <div class="row row-sm-mg-t-10">
    <div class="col-12">
      <div class="row mg-b-10">
        <div class="col-12">
          <div id="carouselEvent" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                
                
                    <?php $i = 1; foreach($data as $row) : ?>
                    <?php if($i == 1) : ?>
                      <div class="carousel-item active">
                  <div class="row">
                    <?php elseif($i % 2 != 0) : ?>
                    <div class="carousel-item">
                  <div class="row">
                  <?php endif; ?>
                    <div class="col-4 <?php echo ($i % 2 != 0 ? 'col-md-offset-2' : '') ?>">
                      <div class="card">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-12">
                              <div class="text-center">
                                <img src="<?php echo base_url('public/images/events/'.$row->banner) ?>" alt="" class="img-responsive center-block" style="width:200px;height:300px;">
                              </div>
                            </div>
                          </div>
                          <div class="row mg-t-10">
                            <div class="col-12 text-center">
                              <h3><?php echo $row->title ?></h3>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-12 text-center">
                              <h4><?php echo $row->place ?></h4>
                            </div>
                          </div>
                          <div class="row mg-t-20">
                            <div class="col-12 text-center">
                              <h3><a href="<?php echo $row->link ?>">DAFTAR</a></h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php if($i % 2 == 0) : ?>

                  </div>
                </div> <?php endif; ?>
                    <?php $i++; endforeach; ?>
                    


              </div>
            </div>
              <a class="carousel-control-prev" href="#carouselEvent" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselEvent" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row mg-t-70" style="position:fixed; bottom:5px;">
    <div class="col-2">
      <div class="card card-body pd-x-10 pd-y-0">
        <h1 class="card-text text-right"><a href="#">News</a></h1>
      </div>
    </div>
    <div class="col-10">
      <div class="card card-body pd-x-0 pd-y-0">
        <h1 class="card-text"><marquee behavior="" direction="left" scrollamount="12"><?php echo running_text() ?></marquee></h1>
      </div>
    </div>
  </div>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<?php $this->load->view('template/scripts') ?>
<script>
  setInterval(function() {
    if($('blink').css('opacity') == "0") {
      $('blink').css('opacity', 1);
    } else {
      $('blink').css('opacity', 0);
    }
  }, 400);
</script>
</body>
</html>
