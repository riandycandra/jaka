<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Digital Signage STMIK Rosma - Event</title>

  <link rel="shortcut icon" type="image/png" href="http://localhost/jaka/public/images/icon.jpg"/>

  <!-- vendor css -->
  <link href="http://localhost/jaka//public/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="http://localhost/jaka//public/css/bootstrap.css" rel="stylesheet">
  <link href="http://localhost/jaka//public/lib/select2/css/select2.min.css" rel="stylesheet">
  <link href="http://localhost/jaka//public/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="http://localhost/jaka//public/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
  <link href="http://localhost/jaka//public/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
  <link href="http://localhost/jaka//public/lib/rickshaw/rickshaw.min.css" rel="stylesheet">
  <link href="http://localhost/jaka//public/lib/chartist/chartist.css" rel="stylesheet">
  <link href="http://localhost/jaka//public/lib/datatables/jquery.dataTables.css" rel="stylesheet">
  <link href="http://localhost/jaka//public/lib/summernote/summernote-bs4.css" rel="stylesheet">


    <!-- <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="../lib/rickshaw/rickshaw.min.css" rel="stylesheet"> -->

    <!-- Bracket CSS -->
    <!-- <link rel="stylesheet" href="http://localhost/jaka//public/css/rangeslider.css"> -->
    <link rel="stylesheet" href="http://localhost/jaka//public/css/searchbox.css">
    <link rel="stylesheet" href="http://localhost/jaka//public/css/bracket.css">
    <link rel="stylesheet" href="http://localhost/jaka//public/css/bootstrap-datetimepicker.min.css">
    <script src="http://localhost/jaka//public/lib/jquery/jquery.js"></script>
    <style>
      body {
        overflow-x: hidden;
      }
    </style></head>

    <body>
      <div class="row row-sm">
        <div class="col-12">
          <div class="card card-body pd-y-20">
            <h1 class="card-text text-center"><a href="http://localhost/jaka/">DIGITAL SIGNAGE - STMIK ROSMA</a></h1>
          </div>
        </div>
      </div>

      <div class="row row-sm mg-t-20 mg-b-20">
        <div class="col-12">
          <div class="text-center"><h2>EVENT</h2></div>
        </div>
      </div>

      <div class="row row-sm-mg-t-10">
        <div class="col-12">
          <div class="row mg-b-10">
            <div class="col-12">
              <div id="carouselEvent" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <div class="row">
                      <div class="col-4 col-md-offset-2">
                        <div class="card">
                          <div class="card-body">
                            <div class="row">
                              <div class="col-12">
                                <div class="text-center">
                                  <img src="https://picsum.photos/200/300" alt="" class="img-responsive center-block">
                                </div>
                              </div>
                            </div>
                            <div class="row mg-t-10">
                              <div class="col-12 text-center">
                                <h3>Seminar e-Commerce</h3>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-12 text-center">
                                <h4>Hotel Mercure</h4>
                              </div>
                            </div>
                            <div class="row mg-t-20">
                              <div class="col-12 text-center">
                                <h3><a href="">DAFTAR</a></h3>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-4 ">
                        <div class="card">
                          <div class="card-body">
                            <div class="row">
                              <div class="col-12">
                                <div class="text-center">
                                  <img src="https://picsum.photos/200/300" alt="" class="img-responsive center-block">
                                </div>
                              </div>
                            </div>
                            <div class="row mg-t-10">
                              <div class="col-12 text-center">
                                <h3>Seminar e-Commerce</h3>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-12 text-center">
                                <h4>Hotel Mercure</h4>
                              </div>
                            </div>
                            <div class="row mg-t-20">
                              <div class="col-12 text-center">
                                <h3><a href="">DAFTAR</a></h3>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                                                             
                  <div class="carousel-item">
                    <div class="row">
                      <div class="col-4 col-md-offset-2">
                        <div class="card">
                          <div class="card-body">
                            <div class="row">
                              <div class="col-12">
                                <div class="text-center">
                                  <img src="https://picsum.photos/200/300" alt="" class="img-responsive center-block">
                                </div>
                              </div>
                            </div>
                            <div class="row mg-t-10">
                              <div class="col-12 text-center">
                                <h3>Seminar e-Commerce</h3>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-12 text-center">
                                <h4>Hotel Mercure</h4>
                              </div>
                            </div>
                            <div class="row mg-t-20">
                              <div class="col-12 text-center">
                                <h3><a href="">DAFTAR</a></h3>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-4 ">
                        <div class="card">
                          <div class="card-body">
                            <div class="row">
                              <div class="col-12">
                                <div class="text-center">
                                  <img src="https://picsum.photos/200/300" alt="" class="img-responsive center-block">
                                </div>
                              </div>
                            </div>
                            <div class="row mg-t-10">
                              <div class="col-12 text-center">
                                <h3>Seminar e-Commerce</h3>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-12 text-center">
                                <h4>Hotel Mercure</h4>
                              </div>
                            </div>
                            <div class="row mg-t-20">
                              <div class="col-12 text-center">
                                <h3><a href="">DAFTAR</a></h3>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                                                             
                  <div class="carousel-item">
                    <div class="row">
                      <div class="col-4 col-md-offset-2">
                        <div class="card">
                          <div class="card-body">
                            <div class="row">
                              <div class="col-12">
                                <div class="text-center">
                                  <img src="https://picsum.photos/200/300" alt="" class="img-responsive center-block">
                                </div>
                              </div>
                            </div>
                            <div class="row mg-t-10">
                              <div class="col-12 text-center">
                                <h3>Seminar e-Commerce</h3>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-12 text-center">
                                <h4>Hotel Mercure</h4>
                              </div>
                            </div>
                            <div class="row mg-t-20">
                              <div class="col-12 text-center">
                                <h3><a href="">DAFTAR</a></h3>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    <a class="carousel-control-prev" href="#carouselEvent" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselEvent" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row mg-t-70" style="position:fixed; bottom:5px;">
            <div class="col-2">
              <div class="card card-body pd-x-10 pd-y-0">
                <h1 class="card-text text-right"><a href="#">News</a></h1>
              </div>
            </div>
            <div class="col-10">
              <div class="card card-body pd-x-0 pd-y-0">
                <h1 class="card-text"><marquee behavior="" direction="left" scrollamount="12">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias laudantium nemo, eveniet excepturi suscipit amet, illo autem quasi sequi illum quisquam doloremque est magni accusamus quo officia, voluptates temporibus cum modi, reiciendis beatae deleniti. Itaque iure porro accusantium expedita sequi qui harum consequatur incidunt minima ratione eum velit enim, voluptatem repudiandae quam. Deserunt libero praesentium facilis quos iste officiis dignissimos magni, repellat modi vero eius eos eum id tempora placeat consectetur quisquam velit ad. Laudantium quisquam rem repudiandae eum, neque laborum alias blanditiis illo ad tenetur, cum sint, quia placeat sunt quo consequuntur optio iusto similique dolores vel veniam distinctio?
                  Lorem ipsum dolor sit amet
                  Lorem ipsum dolor sit amet
                </marquee></h1>
              </div>
            </div>
          </div>
        </div><!-- br-mainpanel -->
        <!-- ########## END: MAIN PANEL ########## -->


        <script src="http://localhost/jaka//public/lib/popper.js/popper.js"></script>
        <script src="http://localhost/jaka//public/lib/bootstrap/bootstrap.js"></script>
        <script src="http://localhost/jaka//public/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
        <script src="http://localhost/jaka//public/lib/moment/moment.js"></script>
        <script src="http://localhost/jaka//public/lib/jquery-ui/jquery-ui.js"></script>
        <script src="http://localhost/jaka//public/lib/jquery-switchbutton/jquery.switchButton.js"></script>
        <script src="http://localhost/jaka//public/lib/peity/jquery.peity.js"></script>
        <script src="http://localhost/jaka//public/lib/chartist/chartist.js"></script>
        <script src="http://localhost/jaka//public/lib/jquery.sparkline.bower/jquery.sparkline.min.js"></script>
        <script src="http://localhost/jaka//public/lib/d3/d3.js"></script>
        <script src="http://localhost/jaka//public/lib/rickshaw/rickshaw.min.js"></script>
        <script src="http://localhost/jaka//public/lib/select2/js/select2.min.js"></script>
        <script src="http://localhost/jaka//public/lib/summernote/summernote-bs4.min.js"></script>


        <!-- <script src="http://localhost/jaka//public/js/rangeslider.js"></script> -->
        <script src="http://localhost/jaka//public/js/bracket.js"></script>
        <script src="http://localhost/jaka//public/js/sweetalert.min.js"></script>
        <script src="http://localhost/jaka//public/js/ResizeSensor.js"></script>
        <script src="http://localhost/jaka//public/js/dashboard.js"></script>
        <script src="http://localhost/jaka//public/js/bootstrap-datetimepicker.min.js"></script>
        <script src="http://localhost/jaka//public/js/multiselect.js"></script>

        <script src="http://localhost/jaka//public/lib/datatables/jquery.dataTables.js"></script>
        <script src="http://localhost/jaka//public/lib/datatables-responsive/dataTables.responsive.js"></script>
        <script src="http://localhost/jaka//public/vendor/ckeditor/ckeditor.js"></script>
        <script src="http://localhost/jaka//public/vendor/sortable/html5sortable.js"></script>

    <!-- <script src="../lib/rickshaw/rickshaw.min.js"></script>
    <script src="../js/bracket.js"></script>
    <script src="../js/ResizeSensor.js"></script>
    <script src="../js/widgets.js"></script> -->

    <script>

      $('#datatable2, table.datatable').DataTable({
        bLengthChange: false,
        searching: true,
        responsive: true,
        "bSort" : false

      });

      $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd'
      });

      CKEDITOR.replace( 'ckeditor' , {
        filebrowserBrowseUrl: 'http://localhost/jaka//public/vendor/ckfinder/ckfinder.html',
        filebrowserUploadUrl: 'http://localhost/jaka//public/vendor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
      });

      $('[data-toggle=confirm]').click(function(e) {
        url = $(this).attr('data-url');
        vartitle = $(this).attr('data-title');
        varicon = $(this).attr('data-icon');
        vartext = $(this).attr('data-text');
        swal({
          title: vartitle,
          text: vartext,
          icon: varicon,
          buttons: true,
          dangerMode: true,

        })
        .then((willDelete) => {
          if(willDelete) {
            document.location.href=url;
          }
        });
      });

      $('.form-ajax').submit(function(e) {
        if(CKEDITOR)
        {
          for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
          }
        }
        $('input[name=submit]').prop('disabled', true);
        $('.loading').toggle();
        $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          data: new FormData($(this)[0]),
          cache: false,
          contentType: false,
          processData: false,
          success: function(data){
            data = JSON.parse(data);
            if(data.status == 20) {
              swal("Success", data.message, "success").then((value) => { 
                if(data.return_url != '#') {
                  document.location.href=data.return_url
                } 
              });
            } else  {
              swal("Failed", data.message, "error");
            }
            $('.loading').toggle();
            $('input[name=submit]').prop('disabled', false);
          },
          error: function(data) {
            swal(data);
            $('.loading').toggle();
            $('input[name=submit]').prop('disabled', false);
          }
        });
        e.preventDefault();
      });

    </script>

    <script>
      $(function(){
        'use strict'

        // FOR DEMO ONLY
        // menu collapsed by default during first page load or refresh with screen
        // having a size between 992px and 1299px. This is intended on this page only
        // for better viewing of widgets demo.
        $(window).resize(function(){
          minimizeMenu();
        });

        minimizeMenu();

        function minimizeMenu() {
          if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
          } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
          }
        }
      });
    </script><script>
      setInterval(function() {
        if($('blink').css('opacity') == "0") {
          $('blink').css('opacity', 1);
        } else {
          $('blink').css('opacity', 0);
        }
      }, 400);
    </script>
  </body>
  </html>
