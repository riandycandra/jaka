<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends MY_Controller {
	public function __construct()
	{
		
	}

	public function index()
	{
		$now = date('Y-m-d');
		$data['data'] = $this->crud->get_all_where('event', '*', array('status' => '2', 'expired_on >=' => $now),  'date DESC');
		$data['title'] = "Event";
		$this->load->view('event', $data);
	}

}