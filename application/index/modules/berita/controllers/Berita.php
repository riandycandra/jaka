<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends MY_Controller {
	public function __construct()
	{
		$this->load->model('M_berita', 'berita');
	}

	public function index()
	{
		$data['data'] = $this->berita->get_all();
		$data['title'] = "Berita";
		$this->load->view('berita', $data);
	}

	public function detail($id = null)
	{
		$data['data'] = $this->berita->get_detail($id);
		$data['title'] = "Berita";
		$this->load->view('berita', $data);
	}

}