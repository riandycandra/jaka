<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_berita extends CI_Model {

	public function get_all()
	{
		$this->db->select('n.id, n.title, n.content, u.name as author, DATE(created_on) as date');
		$this->db->join('user u', 'n.user_id = u.id', 'INNER');
		$this->db->where(array('n.status' => '2'));
		$this->db->order_by('created_on DESC');
		return $this->db->get('news n', 0, 5)->result();
	}

	public function get_detail($id = null)
	{
		$this->db->select('n.id, n.title, n.content, u.name as author, DATE(created_on) as date');
		$this->db->join('user u', 'n.user_id = u.id', 'INNER');
		$this->db->where(array('n.status' => '2', 'n.id' => $id));
		$this->db->order_by('created_on DESC');
		return $this->db->get('news n', 0, 5)->result();
	}

}