<?php isset($data->link) or redirect(base_url()) ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/head-panel') ?>

  <div class="row row-sm mg-t-10">
    <div class="col-12">
      <div class="card card-body" style="height:600px">
        <div class="card-text">
          <div class="col-md-12 ht-550">
              <iframe src="<?php echo $data->link ?>" frameborder="0" style="height: 100%; width:100%;"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row mg-t-70"  style="position:fixed; bottom:5px;">
    <div class="col-2">
        <div class="card card-body pd-x-10 pd-y-0">
          <h1 class="card-text text-right"><a href="#">News</a></h1>
        </div>
    </div>
    <div class="col-10">
      <div class="card card-body pd-x-0 pd-y-0">
          <h1 class="card-text"><marquee behavior="" direction="left" scrollamount="12">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi laboriosam cum rem fugit quis et, sed laudantium ipsa, reprehenderit aut unde ad tempora expedita libero dolor est repellat, alias incidunt consectetur praesentium quo. Enim, quam autem sunt ipsam tempora rem eum dolorem illo, soluta odio, in blanditiis quis repellat sequi nihil. Sit error deserunt, vero aspernatur, ut velit in hic at, debitis incidunt alias optio architecto consectetur quae facere magnam voluptatibus qui minima atque impedit nam asperiores quidem saepe! Architecto voluptatem itaque aperiam facilis, a aliquam eius adipisci dolore sit vel, sunt iure vitae consequatur id ad pariatur in ipsa.</marquee></h1>
        </div>
    </div>
  </div>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<?php $this->load->view('template/scripts') ?>
<script>
  setInterval(function() {
    if($('blink').css('opacity') == "0") {
      $('blink').css('opacity', 1);
    } else {
      $('blink').css('opacity', 0);
    }
  }, 400);
</script>
</body>
</html>
