<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/head-panel') ?>

  <div class="row row-sm mg-t-10">
    <div class="col-12">
      <?php foreach($menu as $row) : 
        if($row->type == 'eksternal') :
          $link = base_url('menu/detail/'.$row->id);
        else :
          $link = $row->link;
        endif;
         ?>
      <div class="col-md-4">
        <div class="card card-body ht-150 mg-t-30">
          <h1 class="card-text text-center">
            <a href='<?php echo $link ?>'><?php echo $row->title ?></a>
          </h1>
        </div>
      </div>
      <?php endforeach; ?>

    </div>
  </div>

  <div class="row mg-t-70"  style="position:fixed; bottom:5px;">
    <div class="col-2">
        <div class="card card-body pd-x-10 pd-y-0">
          <h1 class="card-text text-right"><a href="#">News</a></h1>
        </div>
    </div>
    <div class="col-10">
      <div class="card card-body pd-x-0 pd-y-0">
          <h1 class="card-text">
            <marquee behavior="" direction="left" scrollamount="12">
              <?php echo running_text() ?>
            </marquee>
          </h1>
        </div>
    </div>
  </div>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<?php $this->load->view('template/scripts') ?>
<script>
  setInterval(function() {
    if($('blink').css('opacity') == "0") {
      $('blink').css('opacity', 1);
    } else {
      $('blink').css('opacity', 0);
    }
  }, 400);
</script>
</body>
</html>
