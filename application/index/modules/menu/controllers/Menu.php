<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {
	public function __construct()
	{
		
	}

	public function index()
	{
		$data['menu'] = $this->crud->get_all('menu', 'title, link, type, id', '`order` ASC');

		$data['title'] = "STMIK Rosma";
		$this->load->view('menu', $data);
	}

	public function detail($id = null)
	{
		$data['data'] = $this->crud->get_where('menu', 'link', array('id' => $id));
		$data['title'] = '';
		$this->load->view('menu_iframe', $data);
	}

}