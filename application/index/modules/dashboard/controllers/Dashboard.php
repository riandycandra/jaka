<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	public function __construct()
	{
		$this->load->model('M_dashboard', 'dashboard');
	}

	public function index()
	{
		$data['title'] = "Home";
		$data['news'] = $this->dashboard->get_news();
		$data['hari_ini'] = $this->dashboard->hari_ini(today());
		$data['keuangan'] = $this->dashboard->get_pengumuman_by_name('keuangan');
		$data['akademik'] = $this->dashboard->get_pengumuman_by_name('akademik');
		$data['prodi'] = $this->dashboard->get_pengumuman_by_name('prodi');
		$data['lppm'] = $this->dashboard->get_pengumuman_by_name('lppm');
		$data['event'] = $this->dashboard->get_event();
		
		$this->load->view('dashboard', $data);
	}

	public function hari_ini()
	{
		$hari = today();
		$sekarang = date('H.i');

		$result = $this->dashboard->hari_ini($hari, $sekarang);
		// $result = $this->dashboard->hari_ini('Senin', '19.10');
		if(isset($result->hari))
		{
			$response = array('status' => 'ok', 'data' => $result);
		} else {
			$result = $this->dashboard->hari_ini($hari);
			$response = array('status' => 'ok', 'data' => $result);
		}

		echo json_encode($response);
	}

	public function ucapan()
	{
		$b = time();
		$hour = date("G",$b);
		$time = date('H : i');
		$day = today();
		$date = tgl_indo(date('Y-m-d'));

		if ($hour>=0 && $hour<=11)
		{
		$greetings = "Selamat Pagi STMIK Rosma";
		}
		elseif ($hour >=12 && $hour<=14)
		{
		$greetings = "Selamat Siang STMIK Rosma";
		}
		elseif ($hour >=15 && $hour<=17)
		{
		$greetings = "Selamat Sore STMIK Rosma";
		}
		elseif ($hour >=17 && $hour<=18)
		{
		$greetings = "Selamat Petang STMIK Rosma";
		}
		elseif ($hour >=19 && $hour<=23)
		{
		$greetings = "Selamat Malam STMIK Rosma";
		}


		echo "<h3><blink>$time</blink></h3>
                <h4>Hari $day</h4>
                <h4>$date</h4>
                <h4>$greetings</h4>";


	}

}