<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <div class="row row-sm">
    <div class="col-4">

      <div class="row">
        <div class="col-md-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-t-15 pd-b-10">
              <div class="card-title text-center">
                <!-- <h4>Jadwal Kelas Hari Ini</h4>
                <h2 id="today_kelas">A101</h2>
                <h3 id="today_jam">08.30 - 10.00</h3>
                <h3 id="today_matkul">Matkul. Kalkulus</h3> -->
                <div id="carouselHariIni" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <?php $i = 1; foreach($hari_ini as $row) : ?>
                    <div class="carousel-item <?php echo ($i == 1 ? 'active' : '') ?>">
                      <h2 id="today_kelas"><?php echo $row->ruang ?></h2>
                      <h3 id="today_jam"><?php echo $row->jam ?></h3>
                      <h3 id="today_matkul"><?php echo $row->matakuliah ?></h3>
                    </div>
                    <?php $i++; endforeach; ?>
                  </div>
                  <a class="carousel-control-prev" href="#carouselHariIni" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselHariIni" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row mg-t-20">
        <div class="col-md-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-t-15 pd-b-10">
              <div class="card-title text-center" id="ucapan">
                <h3><blink></blink></h3>
                <h4></h4>
                <h4></h4>
                <h4></h4>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row mg-t-20">
        <div class="col-md-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-x-10 pd-t-30 pd-b-15">
              <div class="card-body">
                <div id="carouselBerita" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <?php $i = 1; foreach($news as $row) : ?>
                    <div class="carousel-item <?php echo ($i == 1 ? 'active' : '') ?>">
                      <h4 class="text-center"><a href="<?php echo base_url('berita/detail/'.$row->id) ?>"><?php echo strip_tags($row->title, 'p') ?></a></h4>
                    </div>
                    <?php $i++; endforeach; ?>
                  </div>
                  <a class="carousel-control-prev" href="#carouselBerita" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselBerita" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row mg-t-20">
        <div class="col-md-12">
          <div class="card pd-0 bd-0 shadow-base" onclick="document.location.href='<?php echo base_url('menu') ?>';">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="card-body">
                <h2 class="text-center">MENU UTAMA</h2>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>

    <div class="col-8">

      <div class="row">
        <div class="col-md-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-x-0 pd-t-0 pd-b-0">
              <div class="card-body">
                <div id="carouselEvent" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <?php $i = 1; foreach($event as $row) : ?>
                    <div class="carousel-item <?php echo ($i == 1 ? 'active' : '') ?>">
                      <a href="<?php echo base_url('event') ?>"><center><img src="<?php echo base_url('public/images/events/'.$row->banner) ?>" class="img-responsive" alt="" style="width:750px;height:137px;"></center></a>
                    </div>
                    <?php $i++; endforeach; ?>
                  </div>
                  <a class="carousel-control-prev" href="#carouselEvent" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselEvent" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row mg-t-20">
        <div class="col-md-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-x-0 pd-t-0 pd-b-0">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-8">
                    <h2>Informasi / Pengumuman</h2>
                    <div class="col-md-3">
                      <div class="card card-body pd-x-0 pd-y-0">
                        <p class="card-text text-center"><a href="#" id="btnKeuangan">Keuangan</a></p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="card card-body pd-x-0 pd-y-0">
                        <p class="card-text text-center"><a href="#" id="btnAkademik">Akademik</a></p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="card card-body pd-x-0 pd-y-0">
                        <p class="card-text text-center"><a href="#" id="btnProdi">Prodi</a></p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="card card-body pd-x-0 pd-y-0">
                        <p class="card-text text-center"><a href="#" id="btnLPPM">LPPM</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 pull-left pd-t-20">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Cari sesuatu...">
                      <span class="input-group-btn">
                        <button class="btn bd bg-white tx-gray-600 form-control" style="width: 50px;" type="button"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="row pd-t-100 ht-400">
                  <div class="col-md-12">
                    <div id="carouselPengumuman_keuangan" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner pd-l-70 pd-r-70">
                        <?php $i = 1; foreach($keuangan as $row) : ?>
                        <div class="carousel-item <?php echo ($i == 1 ? 'active' : '') ?>">
                          <center>
                            <a href="<?php echo base_url('pengumuman/detail/'.$row->id) ?>"><h1><?php echo $row->title ?></h1></a>
                            <h4><?php echo subword(strip_tags($row->content, 'p')) ?></h4>
                          </center>
                        </div>
                        <?php $i++; endforeach; ?>
                      </div>
                      <a class="carousel-control-prev" href="#carouselPengumuman_keuangan" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselPengumuman_keuangan" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                    <div id="carouselPengumuman_prodi" class="carousel slide" data-ride="carousel" style="display: none">
                      <div class="carousel-inner pd-l-70 pd-r-70">
                        <?php $i = 1; foreach($prodi as $row) : ?>
                        <div class="carousel-item <?php echo ($i == 1 ? 'active' : '') ?>">
                          <center>
                            <a href="<?php echo base_url('pengumuman/detail/'.$row->id) ?>"><h1><?php echo $row->title ?></h1></a>
                            <h4><?php echo subword(strip_tags($row->content, 'p')) ?></h4>
                          </center>
                        </div>
                        <?php $i++; endforeach; ?>
                      </div>
                      <a class="carousel-control-prev" href="#carouselPengumuman_prodi" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselPengumuman_prodi" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                    <div id="carouselPengumuman_lppm" class="carousel slide" data-ride="carousel" style="display: none">
                      <div class="carousel-inner pd-l-70 pd-r-70">
                        <?php $i = 1; foreach($lppm as $row) : ?>
                        <div class="carousel-item <?php echo ($i == 1 ? 'active' : '') ?>">
                          <center>
                            <a href="<?php echo base_url('pengumuman/detail/'.$row->id) ?>"><h1><?php echo $row->title ?></h1></a>
                            <h4><?php echo subword(strip_tags($row->content, 'p')) ?></h4>
                          </center>
                        </div>
                        <?php $i++; endforeach; ?>
                      </div>
                      <a class="carousel-control-prev" href="#carouselPengumuman_lppm" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselPengumuman_lppm" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                    <div id="carouselPengumuman_akademik" class="carousel slide" data-ride="carousel" style="display: none">
                      <div class="carousel-inner pd-l-70 pd-r-70">
                        <?php $i = 1; foreach($akademik as $row) : ?>
                        <div class="carousel-item <?php echo ($i == 1 ? 'active' : '') ?>">
                          <center>
                            <a href="<?php echo base_url('pengumuman/detail/'.$row->id) ?>"><h1><?php echo $row->title ?></h1></a>
                            <h4><?php echo subword(strip_tags($row->content, 'p')) ?></h4>
                          </center>
                        </div>
                        <?php $i++; endforeach; ?>
                      </div>
                      <a class="carousel-control-prev" href="#carouselPengumuman_akademik" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselPengumuman_akademik" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row mg-t-10"  style="position:fixed; bottom:5px;">
    <div class="col-2">
        <div class="card card-body pd-x-10 pd-y-0">
          <h1 class="card-text text-right"><a href="#">News</a></h1>
        </div>
    </div>
    <div class="col-10">
      <div class="card card-body pd-x-0 pd-y-0">
          <h1 class="card-text"><marquee behavior="" direction="left" scrollamount="12">
            <?php echo running_text() ?>
          </marquee></h1>
        </div>
    </div>
  </div>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<?php $this->load->view('template/scripts') ?>
<script>
  $.ajax({
    type: 'GET',
    url: '<?php echo base_url('dashboard/ucapan') ?>',
    success: function(data) {
      $('#ucapan').html(data);
    }
  })

  $('#btnKeuangan').click(function(e) {
    $('#carouselPengumuman_keuangan').show();
    $('#carouselPengumuman_lppm, #carouselPengumuman_akademik, #carouselPengumuman_prodi').hide();
    e.preventDefault();
  });

  $('#btnLPPM').click(function(e) {
    $('#carouselPengumuman_lppm').show();
    $('#carouselPengumuman_keuangan, #carouselPengumuman_akademik, #carouselPengumuman_prodi').hide();
    e.preventDefault();
  });

  $('#btnAkademik').click(function(e) {
    $('#carouselPengumuman_akademik').show();
    $('#carouselPengumuman_lppm, #carouselPengumuman_keuangan, #carouselPengumuman_prodi').hide();
    e.preventDefault();
  });

  $('#btnProdi').click(function(e) {
    $('#carouselPengumuman_prodi').show();
    $('#carouselPengumuman_lppm, #carouselPengumuman_akademik, #carouselPengumuman_keuangan').hide();
    e.preventDefault();
  });
  
  setInterval(function() {
    if($('blink').css('opacity') == "0") {
      $('blink').css('opacity', 1);
    } else {
      $('blink').css('opacity', 0);
    }
  }, 400);

  // setInterval(function() {
  //   $.ajax({
  //     type: 'GET',
  //     url: '<?php echo base_url('dashboard/hari_ini') ?>',
  //     success: function(data) {
  //       data = JSON.parse(data);
  //       if(data.status == 'ok')
  //       {
  //         $('#today_matkul').text(data.data.matakuliah);
  //         $('#today_jam').text(data.data.jam);
  //         $('#today_kelas').text(data.data.ruang);
  //       }
  //     }
  //   })
  // }, 1000);

  setInterval(function() {
    $.ajax({
      type: 'GET',
      url: '<?php echo base_url('dashboard/ucapan') ?>',
      success: function(data) {
        $('#ucapan').html(data);
      }
    })
  }, 5000);
</script>
</body>
</html>
