<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {

	public function hari_ini($hari = null, $jam = null)
	{
		if($jam != null)
		{
			$jam = "AND jam.range_jam LIKE '$jam%'";
		}
		return $this->db->query("SELECT `hari`.`nama` AS hari , `ruang`.`nama` AS ruang , `jam`.`range_jam` AS jam , `matakuliah`.`nama` AS matakuliah FROM `penjadwalan_genetik_web`.`pengampu` INNER JOIN `penjadwalan_genetik_web`.`dosen` ON(`pengampu`.`kode_dosen` = `dosen`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`jadwalkuliah` ON (`jadwalkuliah`.`kode_pengampu` = `pengampu`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`ruang` ON (`jadwalkuliah`.`kode_ruang` = `ruang`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`jam` ON (`jadwalkuliah`.`kode_jam` = `jam`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`hari` ON (`jadwalkuliah`.`kode_hari` = `hari`.`kode`) INNER JOIN `penjadwalan_genetik_web`.`matakuliah` ON (`pengampu`.`kode_mk` = `matakuliah`.`kode`) WHERE hari.nama LIKE '%$hari%' $jam")->result();
	}

	public function get_news()
	{
		$this->db->select('n.id, n.title, n.content');
		$this->db->join('category c', 'c.id = n.category_id', 'INNER');
		$this->db->order_by('n.created_on DESC');
		$this->db->where(array('n.status' => '2'));
		return $this->db->get('news n', 0, 5)->result();
	}

	public function get_pengumuman_by_name($name = null)
	{
		$now = date('Y-m-d');
		return $this->db->query("SELECT `pengumuman`.`title` , `pengumuman`.`id` , `pengumuman`.`content` FROM `penjadwalan_genetik_web`.`pengumuman` INNER JOIN `penjadwalan_genetik_web`.`user` ON(`pengumuman`.`user_id` = `user`.`id`) WHERE user.name LIKE '%$name%' AND pengumuman.status = '2' AND expired_on >= '$now' ORDER BY created_on DESC LIMIT 0,5;")->result();
	}

	public function get_event()
	{
		$now = date('Y-m-d');
		return $this->db->query("SELECT * FROM event WHERE expired_on >= '$now' ORDER BY date DESC LIMIT 0,5")->result();
	}

}