<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Digital Signage STMIK Rosma - <?php echo $title ?></title>

    <link rel="shortcut icon" type="image/png" href="<?php echo BASE_URL.'public/images/icon.jpg' ?>"/>

    <!-- vendor css -->
    <link href="<?php echo BASE_URL ?>/public/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/lib/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/lib/chartist/chartist.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/lib/summernote/summernote-bs4.css" rel="stylesheet">

    
    <!-- <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="../lib/rickshaw/rickshaw.min.css" rel="stylesheet"> -->

    <!-- Bracket CSS -->
    <!-- <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/rangeslider.css"> -->
    <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/searchbox.css">
    <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/bracket.css">
    <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/bootstrap-datetimepicker.min.css">
    <script src="<?php echo BASE_URL ?>/public/lib/jquery/jquery.js"></script>
    <style>
        body {
            overflow-x: hidden;
        }
    </style>