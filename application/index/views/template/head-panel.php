<div class="row row-sm">
	<div class="col-12">
		<div class="card card-body pd-y-20">
			<h1 class="card-text text-center"><a href="<?php echo base_url() ?>">DIGITAL SIGNAGE - STMIK ROSMA</a></h1>
		</div>
	</div>
</div>

<div style="position: fixed; top: 20px; left: 10px; display: none;" id="back-arrow" onclick="history.go(-1)">
	<span class="tx-30 tx-bold">
		<i class="fa fa-arrow-left"></i> BACK
	</span>
</div>