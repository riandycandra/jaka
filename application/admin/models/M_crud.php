<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud extends CI_Model {
	
	function get_all($table, $select, $order_by = null) {
		$this->db->select($select);
		$this->db->order_by($order_by);
		return $this->db->get($table)->result();
	}

	function get_all_where($table, $select, $where, $order_by = null) {
		$this->db->select($select);
		$this->db->where($where);
		$this->db->order_by($order_by);
		return $this->db->get($table)->result();
	}

	function get_where($table, $select, $where = array()) {
		$this->db->select($select);
		$this->db->where($where);
		return $this->db->get($table)->row();
	}

	function insert($table, $data = array()) {
		return $this->db->insert($table, $data);
	}

	function update($table, $data = array(), $where = array()) {
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

	function delete($table, $where) {
		return $this->db->delete($table, $where);
	}

	function execute($query) {
		return $this->db->query($query)->result();
	}
	
}