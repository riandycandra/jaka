<?php

function subword($string)
{
	$s = substr($string, 0, 150);
   $result = substr($s, 0, strrpos($s, ' '));
   return $result;
}

function tgl_indo($tanggal)
{
	$bulan = array (
		1 =>   'Januari',
		2 =>'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function rupiah($angka)
{
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}

function debug($str)
{
	echo '<pre>';
	print_r($str);
	die();
}

function check_session($str_usertype = array(), $current_user = null)
{
	$CI =& get_instance();
	if(in_array($current_user, $str_usertype))
	{
		return true;
	} else {
		return false;
	}
}