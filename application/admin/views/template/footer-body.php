<footer class="br-footer">
        <div class="footer-left">
          <div class="mg-b-2">Copyright &copy; 2019. PT. Pupuk Kujang.</div>
        </div>
      </footer>

<!-- Modal ModalImportPegawai-->
<div class="modal fade" id="ModalImportPegawai" tabindex="-1" role="dialog" aria-labelledby="labelModalImportPegawai" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="labelModalImportPegawai">Import Data Pegawai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-layout form-layout-4">
        	<form action="<?php echo base_url('pegawai/import') ?>" class="form-ajax" method="post" enctype="multipart/form-data">
        		<div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label">Upload File<span class="tx-danger">*</span></label>
                  <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                    <input type="file" name="file">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label"></label>
                  <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <a href="<?php echo base_url('assets/files/sample_pegawai.xlsx') ?>" class="btn btn-warning"><i class="fa fa-download"></i> Download Sample File</a>
                  </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label"></label>
                  <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input type="submit" name="submit" value="Submit" class="btn btn-info">
                    &nbsp;
                    <i class="fa fa-refresh fa-spin loading" style="display: none;"></i>
                  </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
        	</form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal ModalImportDepartemen-->
<div class="modal fade" id="ModalImportDepartemen" tabindex="-1" role="dialog" aria-labelledby="labelModalImportDepartemen" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="labelModalImportDepartemen">Import Data Departemen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-layout form-layout-4">
          <form action="<?php echo base_url('departemen/import') ?>" class="form-ajax" method="post" enctype="multipart/form-data">
            <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label">Upload File<span class="tx-danger">*</span></label>
                  <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                    <input type="file" name="file">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label"></label>
                  <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <a href="<?php echo base_url('assets/files/sample_departemen.xlsx') ?>" class="btn btn-warning"><i class="fa fa-download"></i> Download Sample File</a>
                  </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label"></label>
                  <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input type="submit" name="submit" value="Submit" class="btn btn-info">
                    &nbsp;
                    <i class="fa fa-refresh fa-spin loading" style="display: none;"></i>
                  </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal ModalImportPegawai-->
<div class="modal fade" id="ModalImportUnitkerja" tabindex="-1" role="dialog" aria-labelledby="labelModalImportUnitkerja" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="labelModalImportUnitkerja">Import Data Pegawai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-layout form-layout-4">
          <form action="<?php echo base_url('unitkerja/import') ?>" class="form-ajax" method="post" enctype="multipart/form-data">
            <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label">Upload File<span class="tx-danger">*</span></label>
                  <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                    <input type="file" name="file">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label"></label>
                  <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <a href="<?php echo base_url('assets/files/sample_pegawai.xlsx') ?>" class="btn btn-warning"><i class="fa fa-download"></i> Download Sample File</a>
                  </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label"></label>
                  <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input type="submit" name="submit" value="Submit" class="btn btn-info">
                    &nbsp;
                    <i class="fa fa-refresh fa-spin loading" style="display: none;"></i>
                  </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
          </form>
        </div>
      </div>
    </div>
  </div>
</div>