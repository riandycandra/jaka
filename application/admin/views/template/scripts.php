
<script src="<?php echo BASE_URL ?>/public/lib/popper.js/popper.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/bootstrap/bootstrap.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/moment/moment.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/peity/jquery.peity.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/chartist/chartist.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/jquery.sparkline.bower/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/d3/d3.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/rickshaw/rickshaw.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/select2/js/select2.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/summernote/summernote-bs4.min.js"></script>


<!-- <script src="<?php echo BASE_URL ?>/public/js/rangeslider.js"></script> -->
<script src="<?php echo BASE_URL ?>/public/js/bracket.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/ResizeSensor.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/dashboard.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/multiselect.js"></script>

<script src="<?php echo BASE_URL ?>/public/lib/datatables/jquery.dataTables.js"></script>
<script src="<?php echo BASE_URL ?>/public/lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="<?php echo BASE_URL ?>/public/vendor/ckeditor/ckeditor.js"></script>
<script src="<?php echo BASE_URL ?>/public/vendor/sortable/html5sortable.js"></script>

    <!-- <script src="../lib/rickshaw/rickshaw.min.js"></script>
    <script src="../js/bracket.js"></script>
    <script src="../js/ResizeSensor.js"></script>
    <script src="../js/widgets.js"></script> -->

    <script>

      $('#datatable2, table.datatable').DataTable({
        bLengthChange: false,
        searching: true,
        responsive: true,
        "bSort" : false

      });

      $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd'
      });

      CKEDITOR.replace( 'ckeditor' , {
        filebrowserBrowseUrl: '<?php echo BASE_URL ?>/public/vendor/ckfinder/ckfinder.html',
        filebrowserUploadUrl: '<?php echo BASE_URL ?>/public/vendor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
      });

      $('[data-toggle=confirm]').click(function(e) {
        url = $(this).attr('data-url');
        vartitle = $(this).attr('data-title');
        varicon = $(this).attr('data-icon');
        vartext = $(this).attr('data-text');
        swal({
          title: vartitle,
          text: vartext,
          icon: varicon,
          buttons: true,
          dangerMode: true,

        })
        .then((willDelete) => {
          if(willDelete) {
            document.location.href=url;
          }
        });
      });

      $('.form-ajax').submit(function(e) {
        if(CKEDITOR)
        {
          for (instance in CKEDITOR.instances) {
              CKEDITOR.instances[instance].updateElement();
          }
        }
        $('input[name=submit]').prop('disabled', true);
        $('.loading').toggle();
        $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          data: new FormData($(this)[0]),
          cache: false,
          contentType: false,
          processData: false,
          success: function(data){
            data = JSON.parse(data);
            if(data.status == 20) {
              swal("Success", data.message, "success").then((value) => { 
                if(data.return_url != '#') {
                  document.location.href=data.return_url
                } 
              });
            } else  {
              swal("Failed", data.message, "error");
            }
            $('.loading').toggle();
            $('input[name=submit]').prop('disabled', false);
          },
          error: function(data) {
            swal(data);
            $('.loading').toggle();
            $('input[name=submit]').prop('disabled', false);
          }
        });
        e.preventDefault();
      });

    </script>

    <script>
      $(function(){
        'use strict'

        // FOR DEMO ONLY
        // menu collapsed by default during first page load or refresh with screen
        // having a size between 992px and 1299px. This is intended on this page only
        // for better viewing of widgets demo.
        $(window).resize(function(){
          minimizeMenu();
        });

        minimizeMenu();

        function minimizeMenu() {
          if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
          } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
          }
        }
      });
    </script>