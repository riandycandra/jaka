<?php 
$penjadwalan = array('dosen', 'matakuliah', 'pengampu', 'ruang', 'jam', 'hari', 'waktu', 'penjadwalan');
 ?>
<div class="br-logo"><a href=""><span>[</span>Digital Signage<span>]</span></a></div>
<div class="br-sideleft overflow-y-auto">
  <label class="sidebar-label pd-x-15 mg-t-20">Navigation</label>
  <div class="br-sideleft-menu">
    
    <a href="<?php echo base_url() ?>" class="br-menu-link <?php echo ($menu == "dashboard" ? "active" : "") ?>">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
        <span class="menu-item-label">Dashboard</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    
    <?php $allowed_user = array('admin');
    if(check_session($allowed_user, $this->session->userdata('usertype'))) : ?>
    <a href="<?php echo base_url('siteconfiguration') ?>" class="br-menu-link <?php echo ($menu == "siteconfiguration" ? "active" : "") ?>">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-ios-settings tx-22"></i>
        <span class="menu-item-label">Master Data Kampus</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
  

    <a href="#" class="br-menu-link <?php echo ($menu == "penjadwalan" && (in_array($submenu, $penjadwalan)) ? "active show-sub" : "") ?>">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-ios-list-outline tx-24"></i>
        <span class="menu-item-label">Penjadwalan</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
      </div>
    </a> 
    <ul class="br-menu-sub nav flex-column">
      <li class="nav-item"><a href="<?php echo base_url('dosen') ?>" class="nav-link <?php echo ($submenu == 'dosen' ? 'active' : '') ?>">Dosen</a></li>
      <li class="nav-item"><a href="<?php echo base_url('matakuliah') ?>" class="nav-link <?php echo ($submenu == 'matakuliah' ? 'active' : '') ?>">Mata Kuliah</a></li>
      <li class="nav-item"><a href="<?php echo base_url('pengampu') ?>" class="nav-link <?php echo ($submenu == 'pengampu' ? 'active' : '') ?>">Pengampu</a></li>
      <li class="nav-item"><a href="<?php echo base_url('ruang') ?>" class="nav-link <?php echo ($submenu == 'ruang' ? 'active' : '') ?>">Ruangan</a></li>
      <li class="nav-item"><a href="<?php echo base_url('jam') ?>" class="nav-link <?php echo ($submenu == 'jam' ? 'active' : '') ?>">Jam</a></li>
      <li class="nav-item"><a href="<?php echo base_url('hari') ?>" class="nav-link <?php echo ($submenu == 'hari' ? 'active' : '') ?>">Hari</a></li>
      <li class="nav-item"><a href="<?php echo base_url('waktu') ?>" class="nav-link <?php echo ($submenu == 'waktu' ? 'active' : '') ?>">Waktu Tidak Bersedia</a></li>
      <li class="nav-item"><a href="<?php echo base_url('penjadwalan') ?>" class="nav-link <?php echo ($submenu == 'penjadwalan' ? 'active' : '') ?>">Penjadwalan</a></li>
    </ul>
    <?php endif; ?>
  
    <?php $allowed_user = array('admin', 'author');
    if(check_session($allowed_user, $this->session->userdata('usertype'))) : ?>
    <a href="#" class="br-menu-link <?php echo ($menu == "event" && ($submenu == 'form' || $submenu == 'list') ? "active show-sub" : "") ?>">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-ios-calendar-outline tx-24"></i>
        <span class="menu-item-label">Event</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
      </div>
    </a> 
    <ul class="br-menu-sub nav flex-column">
      <li class="nav-item "><a href="<?php echo base_url('event/add') ?>" class="nav-link <?php echo ($submenu == 'form' ? 'active' : '') ?>">Tambah Event</a></li>
      <li class="nav-item "><a href="<?php echo base_url('event') ?>" class="nav-link <?php echo ($submenu == 'list' ? 'active' : '') ?>">Data Event</a></li>
    </ul>
  

    <a href="#" class="br-menu-link <?php echo ($menu == "berita" && ($submenu == 'form' || $submenu == 'list' || $submenu == 'kategori') ? "active show-sub" : "") ?>">
      <div class="br-menu-item">
        <i class="menu-item-icon fa fa-newspaper-o  tx-16"></i>
        <span class="menu-item-label">Berita</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
      </div>
    </a> 
    <ul class="br-menu-sub nav flex-column">
      <li class="nav-item"><a href="<?php echo base_url('kategori') ?>" class="nav-link <?php echo ($submenu == 'kategori' ? 'active' : '') ?>">Kategori</a></li>
      <li class="nav-item"><a href="<?php echo base_url('berita/add') ?>" class="nav-link <?php echo ($submenu == 'form' ? 'active' : '') ?>">Tambah Berita</a></li>
      <li class="nav-item"><a href="<?php echo base_url('berita') ?>" class="nav-link <?php echo ($submenu == 'list' ? 'active' : '') ?>">Data Berita</a></li>
    </ul>
  

    <a href="#" class="br-menu-link <?php echo ($menu == "pengumuman" && ($submenu == 'form' || $submenu == 'list') ? "active show-sub" : "") ?>">
      <div class="br-menu-item">
        <i class="menu-item-icon fa fa-bullhorn  tx-16"></i>
        <span class="menu-item-label">Pengumuman</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
      </div>
    </a> 
    <ul class="br-menu-sub nav flex-column">
      <li class="nav-item"><a href="<?php echo base_url('pengumuman/add') ?>" class="nav-link <?php echo ($submenu == 'form' ? 'active' : '') ?>">Tambah Pengumuman</a></li>
      <li class="nav-item"><a href="<?php echo base_url('pengumuman') ?>" class="nav-link <?php echo ($submenu == 'list' ? 'active' : '') ?>">Data Pengumuman</a></li>
    </ul>
    <?php endif; ?>
    
    <?php $allowed_user = array('admin');
    if(check_session($allowed_user, $this->session->userdata('usertype'))) : ?>
    <a href="<?php echo base_url('menu') ?>" class="br-menu-link <?php echo ($menu == "menu" ? "active" : "") ?>">
      <div class="br-menu-item">
        <i class="menu-item-icon fa fa-list tx-16"></i>
        <span class="menu-item-label">Menu</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    <?php endif; ?>

    <?php $allowed_user = array('admin', 'dosen');
    if(check_session($allowed_user, $this->session->userdata('usertype'))) : ?>
    <a href="<?php echo base_url('resume') ?>" class="br-menu-link <?php echo ($menu == "resume" ? "active" : "") ?>">
      <div class="br-menu-item">
        <i class="menu-item-icon fa fa-sticky-note tx-16"></i>
        <span class="menu-item-label">Ringkasan Materi</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    <?php endif; ?>
    
    <?php $allowed_user = array('admin');
    if(check_session($allowed_user, $this->session->userdata('usertype'))) : ?>
    <a href="<?php echo base_url('user') ?>" class="br-menu-link <?php echo ($menu == "user" ? "active" : "") ?>">
      <div class="br-menu-item">
        <i class="menu-item-icon fa fa-users tx-16"></i>
        <span class="menu-item-label">User</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    <?php endif; ?>

      </div><!-- br-sideleft-menu -->

      

      <br>
    </div>