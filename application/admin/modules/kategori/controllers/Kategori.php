<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{

		$data['title'] = "Data Kategori";
		$data['menu'] = "berita";
		$data['submenu'] = "kategori";
		$data['data'] = $this->crud->get_all('category', '*', 'title ASC');

		$this->load->view('list', $data);
	}

	public function add()
	{
		$data['title'] = "Tambah Data Kategori";
		$data['menu'] = "berita";
		$data['submenu'] = "kategori";
		$data['action'] = base_url('kategori/add_process');
		// $data['dropdown_category'] = $this->dropdown_category();

		$this->load->view('form', $data);
	}

	public function edit($id = null)
	{
		$data['data'] = $this->crud->get_where('kategori', '*', array('id' => $id));
		$data['title'] = "Ubah Data Kategori";
		$data['menu'] = "berita";
		$data['submenu'] = "kategori";
		$data['action'] = base_url('kategori/edit_process');

		$this->load->view('form', $data);
	}

	public function delete($id = null)
	{
		$where = array(
			'id' => $id
		);
		$this->crud->delete('category', $where);

		redirect(base_url('kategori'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('title', 'Nama', 'required|trim');
		
		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['title'] = $this->security->xss_clean($input['title']);
		

			$insert = array(
				'title' => $input['title']
			);

			$this->crud->insert('category', $insert);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Kategori',
					'return_url' => base_url('kategori')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		
		$this->form_validation->set_rules('title', 'Nama', 'required|trim');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

		
			$input['title'] = $this->security->xss_clean($input['title']);
			
			$update = array(
				'title' => $input['title']
			);

			$where = array(
				'id' => $input['id']
			);

			$this->crud->update('category', $update, $where);

			$response = array(
				'status' => 20,
				'message' => 'Berhasil Menyimpan Data Kategori',
				'return_url' => base_url('kategori')
			);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

}