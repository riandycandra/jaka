<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3><?php echo $title ?></h3>
              </div>
              <div class="form-layout form-layout-4">
                <form action="<?php echo $action ?>" class="form-ajax">
                  <input type="hidden" name="id" value="<?php echo (isset($detail->id) ? $detail->id : "") ?>">
                  <div class="row mg-t-20">
                    <label class="col-sm-3 form-control-label">Nama Menu<span class="tx-danger">*</span></label>
                    <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                      <input type="text" name="title" class="form-control" placeholder="Nama Menu" value="<?php echo (isset($detail->title) ? $detail->title : "") ?>">
                    </div>
                  </div>
                  <div class="row mg-t-20">
                    <label class="col-sm-3 form-control-label">Link<span class="tx-danger">*</span></label>
                    <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                      <input type="text" name="link" class="form-control" placeholder="Link" value="<?php echo (isset($detail->link) ? $detail->link : "") ?>">
                    </div>
                  </div>
                  <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Tipe Link <span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <select name="type" class="form-control select2-show-search" data-placeholder="Choose one (with searchbox)">
                      <option value="internal">Internal</option>
                      <option value="internal" <?php echo (isset($detail->type) && $detail->type == 'eksternal' ? 'selected' : '') ?>>Eksternal</option>
                    </select>
                  </div>
                </div>
                  <div class="row mg-t-20">
                    <div class="col-sm-1 col-sm-offset-3">
                      <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                    </div>
                  </div>
                </form>
              </div><!-- card -->
              <br>
            </div>
          </div><!-- row -->

          <div class="card mg-t-20 pd-x-30 pd-y-30 pd-l-150 bd-0 shadow-base">
            <div class="js-grid sortable grid">
              <?php foreach($data as $row) : ?>
                <div class="col-md-3 bg-primary text-center mg-y-10 mg-x-10 pd-y-20 tx-20 rounded-20" data-id="<?php echo $row->id ?>">
                  <a href="<?php echo base_url('menu/edit/'.$row->id) ?>" style="color: #fff;"><?php echo $row->title ?></a>
                </div>
              <?php endforeach; ?>
            </div>
          </div>

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>

      <script>
        sortable('.js-grid', {
          forcePlaceholderSize: true,
          placeholderClass: 'col col-4 border border-maroon'
        });
        sortable('.js-grid')[0].addEventListener('sortupdate', function(e) {
          let apply = sortable('.js-grid', 'serialize');
      
          for(i = 0; i < apply[0].items.length; i++)
          {
            let id = apply[0].items[i].node.dataset.id;
            let index = apply[0].items[i].index;
            // console.log("ID : " + id + ", Posisi : " + index);
            $.ajax({
              type: 'POST',
              url: '<?php echo base_url('menu/edit_order') ?>',
              data: "id=" + id + "&order=" + index
            })
          }
        });
      </script>
    </body>
    </html>
