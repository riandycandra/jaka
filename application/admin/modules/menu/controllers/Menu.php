<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{
		$data['title'] = "Menu";
		$data['menu'] = "menu";
		$data['submenu'] = "";
		$data['action'] = base_url('menu/add_process');

		$data['data'] = $this->crud->get_all('menu', '*', 'order ASC');

		$this->load->view('form', $data);
	}

	public function edit($id = null)
	{
		$data['title'] = "Menu";
		$data['menu'] = "menu";
		$data['submenu'] = "";
		$data['action'] = base_url('menu/edit_process');
		$data['detail'] = $this->crud->get_where('menu', '*', array('id' => $id));

		$data['data'] = $this->crud->get_all('menu', '*', 'order ASC');

		$this->load->view('form', $data);
	}

	public function edit_order()
	{
		$this->form_validation->set_rules('id', 'ID', 'required|trim|numeric');
		$this->form_validation->set_rules('order', 'ORDER', 'required|trim|numeric');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);
			$update = array('order' => $input['order']);
			$where = array('id' => $input['id']);
			$this->crud->update('menu', $update, $where);

			echo "OK";
		} else {
			echo validation_errors();
		}
	}

	public function edit_process()
	{
		$this->form_validation->set_rules('title', 'Nama Menu', 'required|trim');
		$this->form_validation->set_rules('id', 'ID', 'required|trim|numeric');
		$this->form_validation->set_rules('link', 'Link', 'required|trim|valid_url');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);
			$input['title'] = $this->security->xss_clean($input['title']);
			$input['link'] = $this->security->xss_clean($input['link']);
			$input['id'] = $this->security->xss_clean($input['id']);

			$update = array(
				'title' => $input['title'],
				'link' => $input['link'],
			);

			$where = array(
				'id' => $input['id']
			);

			$this->crud->update('menu', $update, $where);

			$response = array(
				'status' => 20,
				'message' => "Berhasil Merubah Menu",
				'return_url' => base_url('menu')
			);
		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);
		}

		echo json_encode($response);
	}

	public function add_process()
	{
		$this->form_validation->set_rules('title', 'Nama Menu', 'required|trim');
		$this->form_validation->set_rules('link', 'Link', 'required|trim|valid_url');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);
			$input['title'] = $this->security->xss_clean($input['title']);
			$input['link'] = $this->security->xss_clean($input['link']);

			$new_order = $this->crud->execute('SELECT `order` FROM menu ORDER BY `order` DESC LIMIT 0, 1')[0]->order;
			$new_order += 1;

			$insert = array(
				'title' => $input['title'],
				'link' => $input['link'],
				'order' => $new_order
			);

			$this->crud->insert('menu', $insert);

			$response = array(
				'status' => 20,
				'message' => "Berhasil Menambah Menu Baru",
				'return_url' => base_url('menu')
			);
		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);
		}

		echo json_encode($response);
	}

	public function tes()
	{
		$new_order = $this->crud->execute('SELECT `order` FROM menu ORDER BY `order` DESC LIMIT 0, 1')[0]->order;
		$new_order += 1;
		debug($new_order);
	}

}