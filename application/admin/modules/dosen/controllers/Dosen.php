<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{

		$data['title'] = "Dosen";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "dosen";
		$data['data'] = $this->crud->get_all('dosen', '*', 'nama ASC');

		$this->load->view('list', $data);
	}

	public function add()
	{
		$data['title'] = "Tambah Data Dosen";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "dosen";
		$data['action'] = base_url('dosen/add_process');
		// $data['dropdown_category'] = $this->dropdown_category();

		$this->load->view('form', $data);
	}

	public function edit($kode = null)
	{
		$data['data'] = $this->crud->get_where('dosen', '*', array('kode' => $kode));
		$data['title'] = "Ubah Data Dosen";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "dosen";
		$data['action'] = base_url('dosen/edit_process');

		$this->load->view('form', $data);
	}

	public function delete($kode = null)
	{
		$where = array(
			'kode' => $kode
		);
		$this->crud->delete('dosen', $where);

		redirect(base_url('dosen'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('nidn', 'NIDN', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('telp', 'Telp', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['nidn'] = $this->security->xss_clean($input['nidn']);
			$input['nama'] = $this->security->xss_clean($input['nama']);
			$input['alamat'] = $this->security->xss_clean($input['alamat']);
			$input['telp'] = $this->security->xss_clean($input['telp']);
			$input['username'] = $this->security->xss_clean($input['username']);
			$input['password'] = substr(md5($input['password']), 0, 15);

			$insert = array(
				'nidn' => $input['nidn'],
				'nama' => $input['nama'],
				'alamat' => $input['alamat'],
				'telp' => $input['telp'],
				'username' => $input['username'],
				'password' => $input['password'],
				'created_on' => date('Y-m-d H:i:s')
			);

			$this->crud->insert('dosen', $insert);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Dosen',
					'return_url' => base_url('berita')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		$this->form_validation->set_rules('nidn', 'NIDN', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('telp', 'Telp', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', 'required|trim');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['nidn'] = $this->security->xss_clean($input['nidn']);
			$input['nama'] = $this->security->xss_clean($input['nama']);
			$input['alamat'] = $this->security->xss_clean($input['alamat']);
			$input['telp'] = $this->security->xss_clean($input['telp']);
			$input['username'] = $this->security->xss_clean($input['username']);

			$update = array(
				'nidn' => $input['nidn'],
				'nama' => $input['nama'],
				'alamat' => $input['alamat'],
				'telp' => $input['telp'],
				'username' => $input['username'],
				'updated_on' => date('Y-m-d H:i:s')
			);

			if(isset($input['password']))
			{
				$update['password'] = substr(md5($input['password']), 0, 15);
			}

			$where = array(
				'kode' => $input['kode']
			);

			$this->crud->update('dosen', $update, $where);

			$response = array(
				'status' => 20,
				'message' => 'Berhasil Menyimpan Data Dosen',
				'return_url' => base_url('dosen')
			);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

}