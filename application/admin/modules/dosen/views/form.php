<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3><?php echo $title ?></h3>
              </div>
              <div class="form-layout form-layout-4">
                <form action="<?php echo $action ?>" class="form-ajax">
                  <input type="hidden" name="kode" value="<?php echo (isset($data) ? $data->kode : "") ?>">
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">NIDN<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="text" name="nidn" class="form-control" placeholder="NIDN" value="<?php echo (isset($data) ? $data->nidn : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Nama<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="text" name="nama" class="form-control" placeholder="Nama" value="<?php echo (isset($data) ? $data->nama : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Alamat<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="text" name="alamat" class="form-control" placeholder="Alamat" value="<?php echo (isset($data) ? $data->alamat : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Telp<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="text" name="telp" class="form-control" placeholder="Telp" value="<?php echo (isset($data) ? $data->telp : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Username<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo (isset($data) ? $data->username : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Password<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="password" name="password" class="form-control" placeholder="Password" value="">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <div class="col-sm-1 col-sm-offset-3">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                  </div>
                  <div class="col-sm-3">
                    <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-danger">BACK</a>
                  </div>
                </div>
                </form>
              </div><!-- card -->
              <br>
            </div>
          </div><!-- row -->

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>
    </body>
    </html>
