<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base mg-t-10">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3>Data Waktu Tidak Bersedia Dosen</h3>
              </div>
              <!-- Editable table -->
              <form action="<?php echo $action ?>" class="form-horizontal form-ajax" method="post">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">
                          <select name="kode_dosen" id="kode_dosen" class="form-control">
                            <?php echo $dosen ?>
                          </select>
                        </div>
                        <div class="col-md-2">
                          <input type="submit" name="submit" value="Submit" class="btn btn-success">
                        </div>
                      
                    </div>
                  </div>
                  <table class="table table-bordered table-responsive-md table-striped text-center mg-t-30">
                    <thead>
                      <tr>
                        <th class="text-center">Hari</th>
                        <th class="text-center">Jam</th>
                        <th class="text-center">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php foreach($hari as $row_hari) : ?>
                        <?php foreach($jam as $row_jam) : ?>
                            <tr>
                              <td><?php echo $row_hari->nama ?></td>
                              <td><?php echo $row_jam->range_jam ?></td>
                              <?php 
                              $status = '';
                              if($this->input->get('kode_dosen'))
                              {
                              $status = '';
                                // debug($waktu_tidak_bersedia);
                                foreach($waktu_tidak_bersedia as $wtb) :
                                  if($wtb->kode_hari === $row_hari->kode && $wtb->kode_jam === $row_jam->kode)
                                  {
                                    $status = "checked";
                                    // debug("checked");
                                  }
                                endforeach;
                              } 
                               ?>
                              
                              <td><label><input type="checkbox" name="tidak_bersedia[]" value="<?php echo @$kode_dosen . '-' .$row_hari->kode . '-' . $row_jam->kode; ?>" <?php echo $status ?>> Tidak Bersedia</label></td>
                            </tr>
                         <?php endforeach; ?>
                     <?php endforeach; ?>
                      <!-- This is our clonable table line -->
                    </tbody>
                  </table>
                </div>
              </div>
              </form>
              <!-- Editable table -->
            </div>
          </div><!-- row -->

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>

      <script>
        $('#kode_dosen').change(function() {
          document.location.href="<?php echo base_url('waktu?kode_dosen=') ?>" + $(this).val();
        });
      </script>
    </body>
    </html>
