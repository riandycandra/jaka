<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Waktu extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{

		if($this->input->get('kode_dosen'))
		{
			$kode_dosen = preg_replace("/[^0-9]/", "", $this->input->get('kode_dosen'));
			// $data['detail_dosen'] = $this->crud->get_where('dosen', 'kode', array('kode' => $kode_dosen));
			$data['kode_dosen'] = $kode_dosen;
			$data['waktu_tidak_bersedia'] = $this->crud->get_all_where('waktu_tidak_bersedia', 'kode_hari, kode_jam', array('kode_dosen' => $kode_dosen));
		}

		$data['title'] = "Data Waktu Tidak Tersedia Dosen";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "waktu";
		$data['action'] = base_url('waktu/edit_process');
		$data['dosen'] = $this->dropdown_dosen(@$kode_dosen);
		$data['hari'] = $this->crud->get_all('hari', 'kode, nama', 'kode ASC');
		$data['jam'] = $this->crud->get_all('jam', 'kode, range_jam', 'range_jam ASC');

		$this->load->view('list', $data);
	}

	public function edit_process()
	{
		
		$this->form_validation->set_rules('kode_dosen', 'Kode', 'required|trim|numeric');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['kode_dosen'] = $this->security->xss_clean($input['kode_dosen']);

			$this->crud->delete('waktu_tidak_bersedia', array('kode_dosen' => $input['kode_dosen']));

			if(isset($input['tidak_bersedia']))
			{
				foreach($input['tidak_bersedia'] as $tidak_bersedia)
				{
					$explode = explode("-", $tidak_bersedia);
					$kode_dosen = $explode[0];
					$kode_hari = $explode[1];
					$kode_jam = $explode[2];

					$insert = array(
						'kode_dosen' => $kode_dosen,
						'kode_hari' => $kode_hari,
						'kode_jam' => $kode_jam,
					);

					$this->crud->insert('waktu_tidak_bersedia', $insert);
				}
			}


			$response = array(
				'status' => 20,
				'message' => 'Berhasil Menyimpan Data Waktu Tidak Bersedia',
				'return_url' => '#'
			);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function dropdown_dosen($selected = null)
	{
		$option = '';
		$data = $this->crud->get_all('dosen','kode, nama', 'nama ASC');
		if($data){
			foreach($data as $row){
				$select = ($selected == $row->kode ? 'selected' : '');
				$option .= '<option value="' . $row->kode . '" '.$select.'>' . $row->nama . '</option>';
			}
		}
		return $option;
	}

}