<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ruang extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{

		$data['title'] = "Data Ruangan";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "ruang";
		$data['data'] = $this->crud->get_all('ruang', '*', 'nama ASC');

		$this->load->view('list', $data);
	}

	public function add()
	{
		$data['title'] = "Tambah Data Ruangan";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "ruang";
		$data['action'] = base_url('ruang/add_process');
		// $data['dropdown_category'] = $this->dropdown_category();

		$this->load->view('form', $data);
	}

	public function edit($kode = null)
	{
		$data['data'] = $this->crud->get_where('ruang', '*', array('kode' => $kode));
		$data['title'] = "Ubah Data Ruangan";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "ruang";
		$data['action'] = base_url('ruang/edit_process');

		$this->load->view('form', $data);
	}

	public function delete($kode = null)
	{
		$where = array(
			'kode' => $kode
		);
		$this->crud->delete('ruang', $where);

		redirect(base_url('ruang'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|trim|numeric');
		
		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['nama'] = $this->security->xss_clean($input['nama']);
			$input['kapasitas'] = $this->security->xss_clean($input['kapasitas']);

			$insert = array(
				'nama' => $input['nama'],
				'kapasitas' => $input['kapasitas'],
				'created_on' => date('Y-m-d H:i:s')
			);

			$this->crud->insert('ruang', $insert);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Ruangan',
					'return_url' => base_url('ruang')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|trim|numeric');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['kapasitas'] = $this->security->xss_clean($input['kapasitas']);
			$input['nama'] = $this->security->xss_clean($input['nama']);
			
			$update = array(
				'nama' => $input['nama'],
				'kapasitas' => $input['kapasitas'],
				'updated_on' => date('Y-m-d H:i:s')
			);

			$where = array(
				'kode' => $input['kode']
			);

			$this->crud->update('ruang', $update, $where);

			$response = array(
				'status' => 20,
				'message' => 'Berhasil Menyimpan Data Ruangan',
				'return_url' => base_url('ruang')
			);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

}