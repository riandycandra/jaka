<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjadwalan extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
		$this->load->model('M_penjadwalan', 'penjadwalan');
		require_once 'Genetik.php';

	}

	public function index()
	{

		$data['title'] = "Penjadwalan";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "penjadwalan";
		$data['action'] = base_url('penjadwalan/create_jadwal');
		$data['data'] = $this->penjadwalan->get_all();

		$this->load->view('list', $data);
	}

	public function create_jadwal()
	{
		//tempat keajaiban dimulai. SEMANGAAAAAATTTTTTT BANZAIIIIIIIIIIIII !
				
		$jenis_semester = $this->input->post('semester_tipe');
		$tahun_akademik = $this->input->post('tahun_akademik');
		$jumlah_populasi = 100; //$this->input->post('jumlah_populasi');
		$crossOver = 0.65; //$this->input->post('probabilitas_crossover');
		$mutasi = 0.30; //$this->input->post('probabilitas_mutasi');
		$jumlah_generasi = 10000; //$this->input->post('jumlah_generasi');
		
		$data['semester_tipe'] = $jenis_semester;
		$data['tahun_akademik'] = $tahun_akademik;
		$data['jumlah_populasi'] = $jumlah_populasi;
		$data['probabilitas_crossover'] = $crossOver;
		$data['probabilitas_mutasi'] = $mutasi;
		$data['jumlah_generasi'] = $jumlah_generasi;
		
	    $rs_data = $this->db->query("SELECT   a.kode,"
                            . "       b.sks,"
                            . "       a.kode_dosen,"
                            . "       b.jenis "
                            . "FROM pengampu a "
                            . "LEFT JOIN matakuliah b "
                            . "ON a.kode_mk = b.kode "
                            . "WHERE b.semester%2 = $jenis_semester "
                            . "      AND a.tahun_akademik = '$tahun_akademik'");
		
		if($rs_data->num_rows() == 0){
			
			$data['msg'] = 'Tidak Ada Data dengan Semester dan Tahun Akademik ini <br>Data yang tampil dibawah adalah data dari proses sebelumnya';
			
			//redirect(base_url() . 'web/penjadwalan','reload');
		}else{
			$this->db->query("TRUNCATE TABLE jadwalkuliah");
			$jenis_jam = array('pagi', 'sore');
			foreach($jenis_jam as $j_jam)
			{

			$genetik = new genetik(
								   $j_jam,
								   $jenis_semester,
								   $tahun_akademik,
								   $jumlah_populasi,
								   $crossOver,
								   $mutasi,
								   //~~~~~~BUG!~~~~~~~
								   /*										   
									1 senin 5
									2 selasa 4
								    3 rabu 3
								    4 kamis 2
								    5 jumat 1										    
								   */
								   5,//kode hari jumat										   
								   '11-12', //kode jam jumat
								   //jam dhuhur tidak dipake untuk sementara
								   12); //kode jam dhuhur
			$genetik->AmbilData();
			$genetik->Inisialisai();
			
			
			
			$found = false;
			
			for($i = 0;$i < $jumlah_generasi;$i++ ){
				$fitness = $genetik->HitungFitness();
				
				//if($i == 100){
				//	var_dump($fitness);
				//	exit();
				//}
				
				$genetik->Seleksi($fitness);
				$genetik->StartCrossOver();
				
				$fitnessAfterMutation = $genetik->Mutasi();
				
				for ($j = 0; $j < count($fitnessAfterMutation); $j++){
					//test here
					if($fitnessAfterMutation[$j] == 1){
						
						
						$jadwal_kuliah = array(array());
						$jadwal_kuliah = $genetik->GetIndividu($j);
						
						
						
						for($k = 0; $k < count($jadwal_kuliah);$k++){
							
							$kode_pengampu = intval($jadwal_kuliah[$k][0]);
							$kode_jam = intval($jadwal_kuliah[$k][1]);
							$kode_hari = intval($jadwal_kuliah[$k][2]);
							$kode_ruang = intval($jadwal_kuliah[$k][3]);
							$this->db->query("INSERT INTO jadwalkuliah(kode_pengampu,kode_jam,kode_hari,kode_ruang) ".
											 "VALUES($kode_pengampu,$kode_jam,$kode_hari,$kode_ruang)");
							
							
						}
						
						//var_dump($jadwal_kuliah);
						//exit();
						
						$found = true;								
					}
					
					if($found){break;}
				}
				
				if($found){break;}
			}
			
			if(!$found){
				$data['msg'] = 'Tidak Ditemukan Solusi Optimal';
			}
			}
			
		}

		redirect(base_url('penjadwalan'));
	}

}