<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="row">
            <div class="col-md-12">
              <form action="<?php echo $action ?>" method="post" class="form-horizontal">
                <div class="form-group">
                  <label for="" class="control-label col-md-2">Semester</label>
                  <div class="col-md-3">
                    <select name="semester_tipe" id="" class="form-control select2-show-search" data-placeholder="Choose one (with searchbox)">
                      <option value="1">GANJIL</option>
                      <option value="0">GENAP</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="control-label col-md-2">Tahun Akademik</label>
                  <div class="col-md-3">
                    <select name="tahun_akademik" id="" class="form-control select2-show-search" data-placeholder="Choose one (with searchbox)">
                      <option value="2017-2018">2017-2018</option>
                      <option value="2018-2019">2018-2019</option>
                      <option value="2019-2020">2019-2020</option>
                      <option value="2020-2021">2020-2021</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-2 col-md-offset-2">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                  </div>
                </div>

              </form>
            </div>
          </div>
          <div class="card pd-0 bd-0 shadow-base mg-t-10">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3>Data Penjadwalan</h3>
              </div>
              <!-- Editable table -->
              <div class="card">
                <div class="card-body">
                  <table class="table table-bordered table-responsive-md table-striped datatable text-center">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Hari</th>
                        <th class="text-center">Jam</th>
                        <th class="text-center">Matakuliah</th>
                        <th class="text-center">SKS</th>
                        <th class="text-center">Semester</th>
                        <th class="text-center">Dosen</th>
                        <th class="text-center">Ruang</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1; foreach($data as $row) : ?>
                      <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $row->hari ?></td>
                        <td><?php echo $row->jam_kuliah ?></td>
                        <td><?php echo $row->nama_mk ?></td>
                        <td><?php echo $row->sks ?></td>
                        <td><?php echo $row->semester ?></td>
                        <td><?php echo $row->dosen ?></td>
                        <td><?php echo $row->ruang ?></td>
                      </tr>
                      <?php $i++; endforeach; ?>
                      <!-- This is our clonable table line -->
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- Editable table -->
            </div>
          </div><!-- row -->

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>
    </body>
    </html>
