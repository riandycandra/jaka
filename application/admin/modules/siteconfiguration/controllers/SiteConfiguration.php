<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteConfiguration extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{

		$data['title'] = "Pengaturan Website";
		$data['menu'] = "siteconfiguration";
		$data['submenu'] = "";
		$data['action'] = base_url('siteconfiguration/edit_process');

		$data['data'] = $this->crud->get_where('config', '*', array('id' => '1'));

		$this->load->view('form', $data);
	}

	public function edit_process()
	{
		$this->form_validation->set_rules('site_name', 'Nama Kampus', 'required|trim');
		$this->form_validation->set_rules('phone', 'Telepon', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'valid_email|trim');
		$this->form_validation->set_rules('address', 'Alamat', 'trim');
		$this->form_validation->set_rules('id', 'ID', 'required|numeric|trim');
		$this->form_validation->set_rules('mahasiswa', 'Jumlah Mahasiswa', 'required|numeric|trim');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['site_name'] = $this->security->xss_clean($input['site_name']);
			$input['phone'] = $this->security->xss_clean($input['phone']);
			$input['address'] = $this->security->xss_clean($input['address']);
			$input['mahasiswa'] = $this->security->xss_clean($input['mahasiswa']);

			$where = array('id' => $input['id']);
			unset($input['id']);
			$update = array(
				'content' => json_encode($input),
				'updated_on' => date('Y-m-d H:i:s')
			);

			if($this->crud->update('config', $update, $where))
			{
				$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Konfigurasi',
					'return_url' => '#'
				);
			} else {
				$response = array(
					'status' => 0,
					'message' => 'Gagal Menyimpan Konfigurasi',
					'return_url' => '#'
				);
			}

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}
}