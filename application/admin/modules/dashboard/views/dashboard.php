<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>
<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-100 mg-b-20 pd-x-30">
      <div class="row row-sm">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3><?php echo $title ?></h3>
              </div><!-- d-flex -->
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-6 col-xl-4">
                      <div class="bg-success rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                          <i class="fa fa-bullhorn tx-70 lh-0 tx-white op-7"></i>
                          <div class="mg-l-20">
                            <p class="tx-15 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Pengumuman</p>
                            <p class="tx-50 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php echo $count['pengumuman'] ?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-xl-4">
                      <div class="bg-warning rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                          <i class="fa fa-newspaper-o tx-70 lh-0 tx-white op-7"></i>
                          <div class="mg-l-20">
                            <p class="tx-15 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Berita</p>
                            <p class="tx-50 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php echo $count['news'] ?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-xl-4">
                      <div class="bg-danger rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                          <i class="fa fa-calendar tx-70 lh-0 tx-white op-7"></i>
                          <div class="mg-l-20">
                            <p class="tx-15 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Event</p>
                            <p class="tx-50 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php echo $count['event'] ?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row mg-t-20">
                    <div class="col-sm-6 col-xl-4">
                      <div class="bg-indigo rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                          <i class="fa fa-building-o tx-70 lh-0 tx-white op-7"></i>
                          <div class="mg-l-20">
                            <p class="tx-15 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Ruang Kelas</p>
                            <p class="tx-50 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php echo $count['ruang'] ?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-xl-4">
                      <div class="bg-info rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                          <i class="fa fa-mortar-board tx-70 lh-0 tx-white op-7"></i>
                          <div class="mg-l-20">
                            <p class="tx-15 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Dosen</p>
                            <p class="tx-50 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php echo $count['dosen'] ?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-xl-4">
                      <div class="bg-purple rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                          <i class="fa fa-users tx-70 lh-0 tx-white op-7"></i>
                          <div class="mg-l-20">
                            <p class="tx-15 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Mahasiswa</p>
                            <p class="tx-50 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php echo $count['mahasiswa'] ?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- card -->
          </div><!-- col-9 -->

        </div><!-- row -->
      </div>
    </div>
    <?php $this->load->view('template/footer-body') ?>
  </div><!-- br-mainpanel -->
  
  <?php $this->load->view('template/scripts') ?>

</body>
</html>

