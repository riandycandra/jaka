<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	public function __construct()
	{
		if(!$this->session->userdata('logged_in'))
		{
			redirect(base_url('auth/login'));
		}
	}

	public function index()
	{

		$data['title'] = "Dashboard";
		$data['menu'] = "dashboard";
		$data['submenu'] = "";

		$data['count'] = array(
			'pengumuman' => $this->crud->get_all('pengumuman', 'count(*) as jumlah', 'id desc')[0]->jumlah,
			'news' => $this->crud->get_all('news', 'count(*) as jumlah', 'id desc')[0]->jumlah,
			'event' => $this->crud->get_all('event', 'count(*) as jumlah', 'id desc')[0]->jumlah,
			'ruang' => $this->crud->get_all('ruang', 'count(*) as jumlah', 'kode desc')[0]->jumlah,
			'dosen' => $this->crud->get_all('dosen', 'count(*) as jumlah', 'kode desc')[0]->jumlah,
			'mahasiswa' => json_decode($this->crud->get_where('config', 'content', array('id' => '1'))->content)->mahasiswa,
		);

		$this->load->view('dashboard', $data);
	}

}