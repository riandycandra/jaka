<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resume extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin', 'dosen');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
		$this->load->model('M_resume', 'resume');
	}

	public function index()
	{
		$data['title'] = "Berita";
		$data['menu'] = "resume";
		$data['submenu'] = "list";

		$data['dropdown_mk'] = $this->dropdown_mk();

		if($this->input->post('submit'))
		{
			$where = array(
				'p.kode_dosen' => $this->session->userdata('id'),
				'p.kode_mk' => $this->input->post('kode_mk')
			);
			$data['data'] = $this->resume->get_resume($where);
		}
			// debug($this->db->last_query());
		// debug($data['data']);

		$this->load->view('list', $data);
	}

	public function add()
	{
		$data['title'] = "Tambah Ringkasan Materi";
		$data['menu'] = "resume";
		$data['submenu'] = "form";
		$data['action'] = base_url('resume/add_process');
		$data['function'] = "add";
		$data['dropdown_mk'] = $this->dropdown_mk();
		$data['dropdown_pertemuan'] = $this->dropdown_pertemuan();

		$this->load->view('form', $data);
	}

	public function edit($id = null)
	{
		$data['data'] = $this->crud->get_where('resume', '*', array('id' => $id));
		$data['title'] = "Ubah Ringkasan Materi";
		$data['menu'] = "resume";
		$data['submenu'] = "form";
		$data['action'] = base_url('resume/edit_process');
		$data['function'] = "edit";
		$data['dropdown_pertemuan'] = $this->dropdown_pertemuan($data['data']->pertemuan_no);

		$this->load->view('form', $data);
	}

	public function delete($id = null)
	{
		$where = array(
			'id' => $id
		);
		$this->crud->delete('resume', $where);

		redirect(base_url('resume'));
	}

	public function add_process()
	{

		$this->form_validation->set_rules('content', 'Ringkasan', 'required|trim');
		$this->form_validation->set_rules('kode_mk', 'Mata Kuliah', 'required|numeric|trim');
		$this->form_validation->set_rules('pertemuan_no', 'Pertemuan Ke', 'required|trim|numeric');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['content'] = $this->security->xss_clean($input['content']);
			$input['kode_mk'] = $this->security->xss_clean($input['kode_mk']);
			$input['pertemuan_no'] = $this->security->xss_clean($input['pertemuan_no']);

			//to get 'id_pengampu'
			$where = array(
				'kode_mk' => $input['kode_mk'],
				'kode_dosen' => $this->session->userdata('id')
			);
			$data = $this->crud->get_all_where('pengampu', 'kode', $where, 'tahun_akademik DESC')[0];
			$id_pengampu = $data->kode;

			$insert = array(
				'content' => $input['content'],
				'pengampu_id' => $id_pengampu,
				'pertemuan_no' => $input['pertemuan_no'],
				'user_id' => $this->session->userdata('id'),
				'created_on' => date('Y-m-d H:i:s'),
				'updated_on' => date('Y-m-d H:i:s')
			);

			$this->crud->insert('resume', $insert);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Ringkasan Materi',
					'return_url' => base_url('resume')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		$this->form_validation->set_rules('content', 'Ringkasan', 'required|trim');
		$this->form_validation->set_rules('id', 'id', 'required|numeric|trim');
		$this->form_validation->set_rules('pertemuan_no', 'Pertemuan Ke', 'required|trim|numeric');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['content'] = $this->security->xss_clean($input['content']);
			$input['pertemuan_no'] = $this->security->xss_clean($input['pertemuan_no']);
			$input['id'] = $this->security->xss_clean($input['id']);

			$update = array(
				'content' => $input['content'],
				'pertemuan_no' => $input['pertemuan_no'],
				'user_id' => $this->session->userdata('id'),
				'updated_on' => date('Y-m-d H:i:s')
			);

			$where = array(
				'id' => $input['id']
			);

			$this->crud->update('resume', $update, $where);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Ringkasan',
					'return_url' => base_url('resume')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function dropdown_mk($selected = null)
	{
		$option = '';
		$where = array('p.kode_dosen' => $this->session->userdata('id'));
		$data = $this->resume->get_mk($where);
		if($data){
			foreach($data as $row){
				$select = ($selected == $row->kode_mk ? 'selected' : '');
				$option .= '<option value="' . $row->kode_mk . '" '.$select.'>' . $row->nama_mk . '</option>';
			}
		}
		return $option;
	}

	public function dropdown_pertemuan($selected = null)
	{
		$jumlah_pertemuan = 12;
		$option = '';
		for($i = 1; $i <= 12; $i++)
		{
			$select = ($selected == $i ? 'selected' : '');
			$option .= "<option value='$i' $select>Pertemuan Ke-$i</option>";
		}

		return $option;
	}
}