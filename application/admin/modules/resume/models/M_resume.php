<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_resume extends CI_Model {

	public function get_resume($where = null)
	{
		$this->db->select('mk.kode as kode_mk, mk.nama as nama_mk, p.kode as pengampu_idm, p.tahun_akademik, p.kode_dosen as kode_dosen, r.pertemuan_no, r.content, r.id');
		$this->db->join('pengampu p', 'p.kode = r.pengampu_id', 'INNER');
		$this->db->join('dosen d', 'd.kode = p.kode_dosen', 'INNER');
		$this->db->join('matakuliah mk', 'mk.kode = p.kode_mk', 'INNER');
		$this->db->order_by('pertemuan_no ASC');
		$this->db->where($where);
		
		return $this->db->get('resume r')->result();
	}

	public function get_mk($where = null)
	{
		$this->db->select('mk.kode as kode_mk, mk.nama as nama_mk');
		$this->db->join('matakuliah mk', 'mk.kode = p.kode_mk', 'INNER');
		$this->db->where($where);

		return $this->db->get('pengampu p')->result();
	}

}