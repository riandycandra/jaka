<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base mg-t-10">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3>Ringkasan Materi</h3>
              </div>
              <!-- Editable table -->
              <div class="card">
                <div class="card-body">
                  <div class="row mg-y-20">
                    <div class="col-md-5">
                      <form action="<?php echo base_url('resume') ?>" class="form-inline" method="POST">
                        <div class="form-group mg-x-20">
                          <select name="kode_mk" id="" class="form-control select2-show-search" data-placeholder="Choose one (with searchbox)">
                            <?php echo $dropdown_mk ?>
                          </select>
                        </div>
                        <input type="submit" name="submit" class="btn btn-primary" value="View">
                      </form>
                    </div>
                    <div class="col-md-1 col-md-offset-6 pull-right">
                      <a href="<?php echo base_url('resume/add') ?>" class="btn btn-success btn-md"><i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                  <table class="table table-bordered table-responsive-md table-striped datatable text-center">
                    <thead>
                      <tr>
                        <th class="text-center">Matakuliah</th>
                        <th class="text-center">Pertemuan Ke</th>
                        <th class="text-center">Isi</th>
                        <th class="text-center">#</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(isset($data) ) { 
                        foreach($data as $row) : ?>
                        <tr>
                          <td class="pt-3-half" ><?php echo $row->nama_mk ?></td>
                          <td class="pt-3-half" ><?php echo $row->pertemuan_no ?></td>
                          <td class="pt-3-half" ><?php echo subword($row->content); ?></td>
                          <td class="pt-3-half">
                            <a href="<?php echo base_url('resume/edit/'.$row->id) ?>" class="btn btn-success btn-rounded btn-sm my-0"><i class="fa fa-pencil"></i></a>
                            <span><button data-toggle="confirm" data-text="Aksi ini tidak dapat dibatalkan" data-ico="warning" data-title="Apakah anda yakin ingin menghapus data ini?" data-url="<?php echo base_url('resume/delete/'.$row->id) ?>" type="button" class="btn btn-danger btn-rounded btn-sm my-0"><i class="fa fa-times"></i></button></span>
                          </td>
                        </tr>
                      <?php endforeach; } ?>
                      <!-- This is our clonable table line -->
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- Editable table -->
            </div>
          </div><!-- row -->

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>
    </body>
    </html>
