<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3><?php echo $title ?></h3>
              </div>
              <div class="form-layout form-layout-4">
                <form action="<?php echo $action ?>" class="form-ajax">
                  <input type="hidden" name="id" value="<?php echo (isset($data) ? $data->id : "") ?>">
                <?php if($function == 'add') : ?>
                  <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Matakuliah <span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <select name="kode_mk" class="form-control select2-show-search" data-placeholder="Choose one (with searchbox)">
                      <?php echo $dropdown_mk ?>
                    </select>
                  </div>
                </div>
                <?php endif; ?>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Pertemuan Ke <span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <select name="pertemuan_no" class="form-control select2-show-search" data-placeholder="Choose one (with searchbox)">
                      <?php echo $dropdown_pertemuan ?>
                    </select>
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label" style="margin-top: -250px;">Isi<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <textarea name="content" id="ckeditor" class="form-control" cols="30" rows="10"><?php echo (isset($data) ? $data->content : "") ?></textarea>
                  </div>
                </div>
                <div class="row mg-t-20">
                  <div class="col-sm-1 col-sm-offset-3">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                  </div>
                  <div class="col-sm-3">
                    <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-danger">BACK</a>
                  </div>
                </div>
                </form>
              </div><!-- card -->
              <br>
            </div>
          </div><!-- row -->

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>

      <script>
        $('#ckeditor').on('keyup', function() {
          alert(CKEDITOR.instances.editor1.getData());
        });
      </script>
    </body>
    </html>
