
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengampu extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
		$this->load->model('M_pengampu', 'pengampu');
	}

	public function index()
	{

		$data['title'] = "Pengampu";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "pengampu";
		$data['data'] = $this->pengampu->get_all();

		$this->load->view('list', $data);
	}

	public function add()
	{
		$data['title'] = "Tambah Data Mata Kuliah";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "penjadwalan";
		$data['action'] = base_url('pengampu/add_process');
		$data['dropdown_mk'] = $this->dropdown_mk();
		$data['dropdown_dosen'] = $this->dropdown_dosen();
		// $data['dropdown_category'] = $this->dropdown_category();

		$this->load->view('form', $data);
	}

	public function edit($kode = null)
	{
		$data['data'] = $this->crud->get_where('pengampu', '*', array('kode' => $kode));
		$data['title'] = "Ubah Data Pengampu";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "pengampu";
		$data['action'] = base_url('pengampu/edit_process');
		$data['dropdown_mk'] = $this->dropdown_mk($data['data']->kode_mk);
		$data['dropdown_dosen'] = $this->dropdown_dosen($data['data']->kode_dosen);

		$this->load->view('form', $data);
	}

	public function delete($kode = null)
	{
		$where = array(
			'kode' => $kode
		);
		$this->crud->delete('pengampu', $where);

		redirect(base_url('pengampu'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('kode_mk', 'Kode MK', 'required|trim');
		$this->form_validation->set_rules('kode_dosen', 'Kode Dosen', 'required|trim');
		$this->form_validation->set_rules('tahun_akademik', 'Tahun Akademik', 'required|trim');
		
		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['kode_mk'] = $this->security->xss_clean($input['kode_mk']);
			$input['kode_dosen'] = $this->security->xss_clean($input['kode_dosen']);
			$input['tahun_akademik'] = $this->security->xss_clean($input['tahun_akademik']);

			$insert = array(
				'kode_mk' => $input['kode_mk'],
				'kode_dosen' => $input['kode_dosen'],
				'tahun_akademik' => $input['tahun_akademik'],
				'created_on' => date('Y-m-d H:i:s'),
				'updated_on' => date('Y-m-d H:i:s')
			);

			$this->crud->insert('pengampu', $insert);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Pengampu',
					'return_url' => base_url('pengampu')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		$this->form_validation->set_rules('kode_mk', 'Kode MK', 'required|trim');
		$this->form_validation->set_rules('kode_dosen', 'Kode Dosen', 'required|trim');
		$this->form_validation->set_rules('tahun_akademik', 'Tahun Akademik', 'required|trim');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['kode_mk'] = $this->security->xss_clean($input['kode_mk']);
			$input['kode_dosen'] = $this->security->xss_clean($input['kode_dosen']);
			$input['tahun_akademik'] = $this->security->xss_clean($input['tahun_akademik']);

			$update = array(
				'kode_mk' => $input['kode_mk'],
				'kode_dosen' => $input['kode_dosen'],
				'tahun_akademik' => $input['tahun_akademik'],
				'updated_on' => date('Y-m-d H:i:s')
			);

			$where = array(
				'kode' => $input['kode']
			);

			$this->crud->update('pengampu', $update, $where);

			$response = array(
				'status' => 20,
				'message' => 'Berhasil Menyimpan Data Pengampu',
				'return_url' => base_url('pengampu')
			);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function dropdown_mk($selected = null)
	{
		$option = '';
		$data = $this->crud->get_all('matakuliah','*', 'nama ASC');
		if($data){
			foreach($data as $row){
				$select = ($selected == $row->kode ? 'selected' : '');
				$option .= '<option value="' . $row->kode . '" '.$select.'>' . $row->nama . '</option>';
			}
		}
		return $option;
	}

	public function dropdown_dosen($selected = null)
	{
		$option = '';
		$data = $this->crud->get_all('dosen','*', 'nama ASC');
		if($data){
			foreach($data as $row){
				$select = ($selected == $row->kode ? 'selected' : '');
				$option .= '<option value="' . $row->kode . '" '.$select.'>' . $row->nama . '</option>';
			}
		}
		return $option;
	}

}