<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengampu extends CI_Model {

	public function get_all()
	{
		$this->db->select('mk.nama as matakuliah, d.nama as dosen, p.tahun_akademik, p.kode');
		$this->db->join('matakuliah mk', 'p.kode_mk = mk.kode', 'INNER');
		$this->db->join('dosen d', 'p.kode_dosen = d.kode', 'INNER');
		return $this->db->get('pengampu p')->result();
	}

}