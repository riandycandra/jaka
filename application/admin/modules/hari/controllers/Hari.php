<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hari extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{

		$data['title'] = "Data Hari";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "hari";
		$data['data'] = $this->crud->get_all('hari', '*', 'nama ASC');

		$this->load->view('list', $data);
	}

	public function add()
	{
		$data['title'] = "Tambah Data Hari";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "hari";
		$data['action'] = base_url('hari/add_process');
		// $data['dropdown_category'] = $this->dropdown_category();

		$this->load->view('form', $data);
	}

	public function edit($kode = null)
	{
		$data['data'] = $this->crud->get_where('hari', '*', array('kode' => $kode));
		$data['title'] = "Ubah Data Hari";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "hari";
		$data['action'] = base_url('hari/edit_process');

		$this->load->view('form', $data);
	}

	public function delete($kode = null)
	{
		$where = array(
			'kode' => $kode
		);
		$this->crud->delete('hari', $where);

		redirect(base_url('hari'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		
		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['nama'] = $this->security->xss_clean($input['nama']);
		

			$insert = array(
				'nama' => $input['nama'],
				'created_on' => date('Y-m-d H:i:s')
			);

			$this->crud->insert('hari', $insert);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Hari',
					'return_url' => base_url('hari')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

		
			$input['nama'] = $this->security->xss_clean($input['nama']);
			
			$update = array(
				'nama' => $input['nama'],
				'updated_on' => date('Y-m-d H:i:s')
			);

			$where = array(
				'kode' => $input['kode']
			);

			$this->crud->update('hari', $update, $where);

			$response = array(
				'status' => 20,
				'message' => 'Berhasil Menyimpan Data Hari',
				'return_url' => base_url('hari')
			);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

}