<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3><?php echo $title ?></h3>
              </div>
              <div class="form-layout form-layout-4">
                <form action="<?php echo $action ?>" class="form-ajax">
                  <input type="hidden" name="id" value="<?php echo (isset($data) ? $data->id : "") ?>">
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Judul<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="text" name="title" class="form-control" placeholder="Judul" value="<?php echo (isset($data) ? $data->title : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label" style="margin-top: -250px;">Deskripsi<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <textarea name="content" id="ckeditor" class="form-control" cols="30" rows="10"><?php echo (isset($data) ? $data->content : "") ?></textarea>
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Tanggal Event<span class="tx-danger">*</span></label>
                  <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                    <input type="text" name="date" class="form-control datepicker" placeholder="Tanggal" value="<?php echo (isset($data) ? $data->date : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Tanggal Kadaluarsa<span class="tx-danger">*</span></label>
                  <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                    <input type="text" name="expired_on" class="form-control datepicker" placeholder="Tanggal Kadaluarsa" value="<?php echo (isset($data) ? $data->expired_on : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Tempat<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="text" name="place" class="form-control" placeholder="Tempat" value="<?php echo (isset($data) ? $data->place : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Link<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="text" name="link" class="form-control" placeholder="Link" value="<?php echo (isset($data) ? $data->link : "") ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-3 form-control-label">Flyer<span class="tx-danger">*</span></label>
                  <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                    <input type="file" name="banner">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <div class="col-sm-9 col-sm-offset-3 mg-t-10 mg-sm-t-0">
                    <?php echo (isset($data) ? "<img src='".BASE_URL.'/public/images/events/'.$data->banner."' style='width:250px;'/>" : "") ?>
                  </div>
                </div>
                <div class="row mg-t-20">
                  <div class="col-sm-1 col-sm-offset-3">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                  </div>
                  <div class="col-sm-3">
                    <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-danger">BACK</a>
                  </div>
                </div>
                </form>
              </div><!-- card -->
              <br>
            </div>
          </div><!-- row -->

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>

      <script>
        $('#ckeditor').on('keyup', function() {
          alert(CKEDITOR.instances.editor1.getData());
        });
      </script>
    </body>
    </html>
