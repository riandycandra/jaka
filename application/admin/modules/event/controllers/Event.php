<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends MY_Controller {

	public function __construct()
	{
		$allowed_user = array('admin', 'author');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{
		// jika admin, tampilkan semua berita, jika bukan, tampilkan berita yang ditulis oleh user yg sedang login
		if($this->session->userdata('usertype') == 'admin')
		{
			$data['data'] = $this->crud->get_all('event', '*, if(status = "2", "Tampil", "Belum Tampil") as status_tampil', 'date DESC');
		} else {
			$where = array(
				'user_id' => $this->session->userdata('id')
			);
			$data['data'] = $this->crud->get_all_where('event', '*, if(status = "2", "Tampil", "Belum Tampil") as status_tampil', $where, 'date DESC');
		}


		$data['title'] = "Event";
		$data['menu'] = "event";
		$data['submenu'] = "list";

		

		$where['status'] = '2';
		$data['data_tampil'] = $this->crud->get_all_where('event', '*', $where, 'date DESC');

		$this->load->view('list', $data);
	}

	// method untuk menyembunyikan event
	public function hide($id = null)
	{
		$where = array('id' => $id);
		$update = array('status' => '0');
		$this->crud->update('event', $update, $where);

		redirect(base_url('event'));
	}

	// method untuk menampilkan event yang tersembunyi

	public function show($id = null)
	{
		$where = array('id' => $id);
		$update = array('status' => '2');
		$this->crud->update('event', $update, $where);

		redirect(base_url('event'));
	}

	public function add()
	{
		$data['title'] = "Tambah Event";
		$data['menu'] = "event";
		$data['submenu'] = "form";
		$data['action'] = base_url('event/add_process');

		$this->load->view('form', $data);
	}

	public function edit($id = null)
	{
		$data['data'] = $this->crud->get_where('event', '*', array('id' => $id));
		$data['title'] = "Ubah Event";
		$data['menu'] = "event";
		$data['submenu'] = "form";
		$data['action'] = base_url('event/edit_process');

		$this->load->view('form', $data);
	}

	public function delete($id = null)
	{
		$where = array(
			'id' => $id
		);
		$this->crud->delete('event', $where);

		redirect(base_url('event'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim');
		$this->form_validation->set_rules('date', 'Date', 'required|trim');
		$this->form_validation->set_rules('expired_on', 'Tanggal Kadaluarsa', 'required|trim');
		$this->form_validation->set_rules('link', 'Link', 'required|trim');
		$this->form_validation->set_rules('place', 'Place', 'required|trim');

		if($this->form_validation->run() === TRUE)
		{

			$config['upload_path']          = ROOT_PATH.'/public/images/events/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png';
            $config['file_name']			= substr(md5(rand(1,100).date('Y-m-d H:i:s')), 0, 15);

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('banner'))
            {
                    $response = array(
						'status' => 0,
						'message' => $this->upload->display_errors(),
						'return_url' => '#'
					);
            }
            else
            {
            	$input = $this->input->post(null, true);
            	$data = $this->upload->data();

				$input['title'] = $this->security->xss_clean($input['title']);
				$input['date'] = $this->security->xss_clean($input['date']);
				$input['expired_on'] = $this->security->xss_clean($input['expired_on']);
				$input['link'] = $this->security->xss_clean($input['link']);
				$input['place'] = $this->security->xss_clean($input['place']);

				$insert = array(
					'title' => $input['title'],
					'content' => $this->input->post('content'),
					'date' => $input['date'],
					'expired_on' => $input['expired_on'],
					'link' => $input['link'],
					'place' => $input['place'],
					'banner' => $data['file_name'],
					'user_id' => $this->session->userdata('id'),
					'created_on' => date('Y-m-d H:i:s')
				);

				$this->crud->insert('event', $insert);

				$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Event',
					'return_url' => base_url('event')
				);

            }


			

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim');
		$this->form_validation->set_rules('date', 'Date', 'required|trim');
		$this->form_validation->set_rules('expired_on', 'Tanggal Kadaluarsa', 'required|trim');
		$this->form_validation->set_rules('link', 'Link', 'required|trim');
		$this->form_validation->set_rules('place', 'Place', 'required|trim');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['title'] = $this->security->xss_clean($input['title']);
			$input['expired_on'] = $this->security->xss_clean($input['expired_on']);
			$input['id'] = $this->security->xss_clean($input['id']);

			$update = array(
				'title' => $input['title'],
				'content' => $this->input->post('content'),
				'date' => $input['date'],
				'expired_on' => $input['expired_on'],
				'place' => $input['place'],
				'updated_on' => date('Y-m-d H:i:s')
			);

			$where = array(
				'id' => $input['id']
			);

			$update['title'] = $this->security->xss_clean($input['title']);
			$update['date'] = $this->security->xss_clean($input['date']);
			$update['expired_on'] = $this->security->xss_clean($input['expired_on']);
			$update['link'] = $this->security->xss_clean($input['link']);
			$update['place'] = $this->security->xss_clean($input['place']);

			if($_FILES['banner']['name'] != '')
			{
				$config['upload_path']          = ROOT_PATH.'/public/images/events/';
	            $config['allowed_types']        = 'gif|jpg|jpeg|png';
	            $config['file_name']			= substr(md5(rand(1,100).date('Y-m-d H:i:s')), 0, 15);

	            $this->load->library('upload', $config);

	            if ( ! $this->upload->do_upload('banner'))
	            {
	                    $response = array(
							'status' => 0,
							'message' => $this->upload->display_errors(),
							'return_url' => '#'
						);
	            }
	            else
	            {
	            	$data = $this->upload->data();
	            	$update['banner'] = $data['file_name'];

	            	$this->crud->update('event', $update, $where);

					$response = array(
						'status' => 20,
						'message' => 'Berhasil Menyimpan Data Event',
						'return_url' => base_url('event')
					);
					
	            }
			} else {
				$this->crud->update('event', $update, $where);

				$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Event',
					'return_url' => base_url('event')
				);
			}

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}
}