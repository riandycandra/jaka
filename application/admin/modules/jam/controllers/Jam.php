<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jam extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{

		$data['title'] = "Data Jam";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "jam";
		$data['data'] = $this->crud->get_all('jam', '*', 'range_jam ASC');

		$this->load->view('list', $data);
	}

	public function add()
	{
		$data['title'] = "Tambah Data Jam";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "jam";
		$data['action'] = base_url('jam/add_process');
		// $data['dropdown_category'] = $this->dropdown_category();

		$this->load->view('form', $data);
	}

	public function edit($kode = null)
	{
		$data['data'] = $this->crud->get_where('jam', '*', array('kode' => $kode));
		$data['title'] = "Ubah Data Jam";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "jam";
		$data['action'] = base_url('jam/edit_process');

		$this->load->view('form', $data);
	}

	public function delete($kode = null)
	{
		$where = array(
			'kode' => $kode
		);
		$this->crud->delete('jam', $where);

		redirect(base_url('jam'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('range_jam', 'Range Jam', 'required|trim');
		$this->form_validation->set_rules('tipe', 'Tipe', 'required|trim|alpha_numeric');
		
		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['range_jam'] = $this->security->xss_clean($input['range_jam']);
			$input['tipe'] = $this->security->xss_clean($input['tipe']);

			$insert = array(
				'range_jam' => $input['range_jam'],
				'tipe' => $input['tipe'],
				'created_on' => date('Y-m-d H:i:s')
			);

			$this->crud->insert('jam', $insert);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Jam',
					'return_url' => base_url('jam')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		
		$this->form_validation->set_rules('range_jam', 'Range Jam', 'required|trim');
		$this->form_validation->set_rules('tipe', 'Tipe', 'required|trim|alpha_numeric');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['range_jam'] = $this->security->xss_clean($input['range_jam']);
			$input['tipe'] = $this->security->xss_clean($input['tipe']);
			
			$update = array(
				'tipe' => $input['tipe'],
				'range_jam' => $input['range_jam'],
				'updated_on' => date('Y-m-d H:i:s')
			);

			$where = array(
				'kode' => $input['kode']
			);

			$this->crud->update('jam', $update, $where);

			$response = array(
				'status' => 20,
				'message' => 'Berhasil Menyimpan Data Jam',
				'return_url' => base_url('jam')
			);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

}