<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base mg-t-10">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3>Data Jam</h3>
                <a href="<?php echo base_url('jam/add') ?>" class="btn btn-success btn-md mg-b-10"><i class="fa fa-plus"></i></a>
              </div>
              <!-- Editable table -->
              <div class="card">
                <div class="card-body">
                  <table class="table table-bordered table-responsive-md table-striped datatable text-center">
                    <thead>
                      <tr>
                        <th class="text-center">Range Jam</th>
                        <th class="text-center">Tipe Kelas</th>
                        <th class="text-center">#</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data as $row) : ?>
                        <tr>
                          <td class="pt-3-half" ><?php echo $row->range_jam ?></td>
                          <td class="pt-3-half" ><?php echo strtoupper($row->tipe) ?></td>
                          <td class="pt-3-half">
                            <a href="<?php echo base_url('jam/edit/'.$row->kode) ?>" class="btn btn-success btn-rounded btn-sm my-0"><i class="fa fa-pencil"></i></a>
                            <span><button data-toggle="confirm" data-text="Aksi ini tidak dapat dibatalkan" data-ico="warning" data-title="Apakah anda yakin ingin menghapus <?php echo $row->range_jam ?>?" data-url="<?php echo base_url('jam/delete/'.$row->kode) ?>" type="button" class="btn btn-danger btn-rounded btn-sm my-0"><i class="fa fa-times"></i></button></span>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <!-- This is our clonable table line -->
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- Editable table -->
            </div>
          </div><!-- row -->

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>
    </body>
    </html>
