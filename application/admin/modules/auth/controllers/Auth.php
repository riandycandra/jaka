<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	// if user access to http://domain.com/auth
	// redirect to login page, login page is on http://domain.com/auth/login
	public function index()
	{
		redirect(base_url('auth/login'));
	}

	// display login form
	public function login()
	{

		$data['action'] = base_url('auth/login_process');

		// check is user has been logged in or not
		// if user has been logged in, redirect to dashboard
		if(@$this->session->userdata('logged_in') == TRUE)
		{
			redirect(base_url('dashboard'));
		}

		// if not, load login form
		$this->load->view('form', $data);
	}

	// method for destroying session
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('auth/login'));
	}

	// process user's submission from login form
	public function login_process()
	{
		// set rules for form validation
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required');

		// check whether validation ends up with no error
		if($this->form_validation->run() === TRUE)
		{

			// get input from user
			$input = $this->input->post(null, true);

			$username = $input['username'];
			$password = substr(md5($input['password']), 0, 15);

			// check login
			$login = $this->check_login($username, $password);

			if($login)
			{
				//set response status with success code (20)
				$response = array(
					'status' => 20,
					'message' => 'Berhasil, Selamat Datang '.$this->session->nama.' !',
					'return_url' => base_url('dashboard')
				);

			} else {
				//set response status with failed code (0)
				$response = array(
					'status' => 0,
					'message' => 'Username atau Password salah!',
					'return_url' => '#'
				);
			}
		} else {

			//set response status with failed code (0)
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	// method for checking login is valid or not
	public function check_login($username = null, $password = null)
	{

		// set where clause
		$where = array(
			'username' => $username,
			'password' => $password
		);

		// get data from database based on user input
		$data = $this->crud->get_where('user', '*', $where);

		// check is data fetched?
		if(isset($data->username))
		{

			// set session
			$session = array(
				'logged_in' => true,
				'id' => $data->id,
				'username' => $data->username,
				'nama' => $data->name,
				'usertype' => $data->usertype
			);
			$this->session->set_userdata($session);

			return true;

		} else {

			// check in table 'dosen'
			$data = $this->crud->get_where('dosen', '*', $where);

			if(isset($data->username))
			{

				// set session
				$session = array(
					'logged_in' => true,
					'id' => $data->kode,
					'username' => $data->username,
					'nama' => $data->nama,
					'usertype' => 'dosen'
				);
				$this->session->set_userdata($session);

				return true;

			} else {
				
				// login failed
				return false;

			}
		}
	}

}