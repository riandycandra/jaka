<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Restricted Area</title>

    <!-- vendor css -->
    <link href="<?php echo BASE_URL ?>/public/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/public/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/bracket.css">
  </head>

  <body >

    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v" style="background: #01506f">
    <form action="<?php echo $action ?>" class="form-ajax">
      <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
        <div class="signin-logo tx-center tx-28 tx-bold tx-inverse"><span class="tx-normal">
      <img src="<?php echo BASE_URL.'/public/images/icon.jpg' ?>" alt="" class="img-responsive">
        </div>
        <div class="tx-center mg-b-60 mg-t-20"><b>Digital Signage<br>STMIK ROSMA</b></div>

        <div class="form-group">
          <input type="text" name="username" class="form-control" placeholder="Enter your username">
        </div><!-- form-group -->
        <div class="form-group">
          <input type="password" name="password" class="form-control" placeholder="Enter your password">
        </div><!-- form-group -->
        <input type="submit" name="submit" value="Sign In" class="btn btn-default btn-block">

      </div><!-- login-wrapper -->
  </form>
    </div><!-- d-flex -->

    <script src="<?php echo BASE_URL ?>/public/lib/jquery/jquery.js"></script>
    <script src="<?php echo BASE_URL ?>/public/lib/popper.js/popper.js"></script>
    <script src="<?php echo BASE_URL ?>/public/lib/bootstrap/bootstrap.js"></script>
    <script src="<?php echo BASE_URL ?>/public/js/sweetalert.min.js"></script>

    <script>
    $(document).ready(function() {
      $('input[name=username]').focus();
    });

    $('form.form-ajax').submit(function(e) {
          $('input[name=submit]').prop('disabled', true);
          $.ajax({
              type: 'POST',
              url: $(this).attr('action'),
              data: new FormData($(this)[0]),
              cache: false,
                contentType: false,
                processData: false,
              success: function(data){
                  data = JSON.parse(data);
                  if(data.status == 20) {
                    swal("Success", data.message, "success").then((value) => { 
                      if(data.return_url != '#') {
                        document.location.href=data.return_url
                      } 
                    });
                  } else  {
                    swal("Failed", data.message, "error");
                  }
              $('input[name=submit]').prop('disabled', false);
              },
              error: function(data) {
                swal(data);
              $('input[name=submit]').prop('disabled', false);
              }
          });
          e.preventDefault();
        });
  </script>

  </body>
</html>
