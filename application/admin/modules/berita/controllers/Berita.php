<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin', 'author');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
		$this->load->model('M_berita', 'berita');
	}

	public function index()
	{
		// jika admin, tampilkan semua berita, jika bukan, tampilkan berita yang ditulis oleh user yg sedang login
		if($this->session->userdata('usertype') == 'admin')
		{
			$where = array(
				'n.status' => '2'
			);
		} else {
			$where = array(
				'n.user_id' => $this->session->userdata('id')
			);
		}


		$data['title'] = "Berita";
		$data['menu'] = "berita";
		$data['submenu'] = "list";

		$data['data_tampil'] = $this->berita->get_all($where);
		$data['data'] = $this->berita->get_all();

		if($this->session->userdata('usertype') == 'admin')
		{
			$this->load->view('list', $data);
		} else {
			$this->load->view('list_user', $data);
		}
	}

	// method untuk menyembunyikan berita
	public function hide($id = null)
	{
		$where = array('id' => $id);
		$update = array('status' => '0');
		$this->crud->update('news', $update, $where);

		redirect(base_url('berita'));
	}

	// method untuk menampilkan berita yang tersembunyi

	public function show($id = null)
	{
		$where = array('id' => $id);
		$update = array('status' => '2');
		$this->crud->update('news', $update, $where);

		redirect(base_url('berita'));
	}

	public function add()
	{
		$data['title'] = "Tambah Berita";
		$data['menu'] = "berita";
		$data['submenu'] = "form";
		$data['action'] = base_url('berita/add_process');
		$data['dropdown_category'] = $this->dropdown_category();

		$this->load->view('form', $data);
	}

	public function edit($id = null)
	{
		$data['data'] = $this->crud->get_where('news', '*', array('id' => $id));
		$data['title'] = "Ubah Berita";
		$data['menu'] = "berita";
		$data['submenu'] = "form";
		$data['action'] = base_url('berita/edit_process');
		$data['dropdown_category'] = $this->dropdown_category($data['data']->category_id);

		$this->load->view('form', $data);
	}

	public function delete($id = null)
	{
		$where = array(
			'id' => $id
		);
		$this->crud->delete('news', $where);

		redirect(base_url('berita'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim');
		$this->form_validation->set_rules('category_id', 'Category', 'required|trim|numeric');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['title'] = $this->security->xss_clean($input['title']);
			$input['category_id'] = $this->security->xss_clean($input['category_id']);

			$insert = array(
				'title' => $input['title'],
				'category_id' => $input['category_id'],
				'content' => $this->input->post('content'),
				'user_id' => $this->session->userdata('id'),
				'created_on' => date('Y-m-d H:i:s')
			);

			$this->crud->insert('news', $insert);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Berita',
					'return_url' => base_url('berita')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim');
		$this->form_validation->set_rules('id', 'id', 'required|numeric|trim');
		$this->form_validation->set_rules('category_id', 'Category', 'required|trim|numeric');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['title'] = $this->security->xss_clean($input['title']);
			$input['category_id'] = $this->security->xss_clean($input['category_id']);
			$input['id'] = $this->security->xss_clean($input['id']);

			$update = array(
				'title' => $input['title'],
				'category_id' => $input['category_id'],
				'content' => $this->input->post('content'),
				// 'user_id' => $this->session->userdata('id'),
				'updated_on' => date('Y-m-d H:i:s')
			);

			$where = array(
				'id' => $input['id']
			);

			$this->crud->update('news', $update, $where);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Berita',
					'return_url' => base_url('berita')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function dropdown_category($selected = null)
	{
		$option = '';
		$data = $this->crud->get_all('category','*', 'title ASC');
		if($data){
			foreach($data as $row){
				$select = ($selected == $row->id ? 'selected' : '');
				$option .= '<option value="' . $row->id . '" '.$select.'>' . $row->title . '</option>';
			}
		}
		return $option;
	}
}