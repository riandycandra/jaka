<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_berita extends CI_Model {

	public function get_all($where = null)
	{
		$this->db->select('n.id, n.title, n.created_on, n.status, c.title as category, u.name, if(n.status = "2", "Tampil", "Belum Tampil") as status_tampil');
		$this->db->join('category c', 'c.id = n.category_id', 'INNER');
		$this->db->join('user u', 'u.id = n.user_id', 'INNER');
		if($where != null)
		{
			$this->db->where($where);
		}

		return $this->db->get('news n')->result();
	}

}