<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3>Ubah Data Pengguna Internal</h3>
              </div>
              <div class="form-layout form-layout-4">
                <form action="<?php echo $action ?>" class="form-ajax">
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label">Nama<span class="tx-danger">*</span></label>
                  <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                    <input type="text" name="name" class="form-control" placeholder="Nama" value="<?php echo $data->name ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label">Username<span class="tx-danger">*</span></label>
                  <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                    <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $data->username ?>">
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label">Password<span class="tx-danger">*</span></label>
                  <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                  </div>
                  <small>kosongkan jika tidak diubah</small>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label">Konfirmasi Password<span class="tx-danger">*</span></label>
                  <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                    <input type="password" name="passconf" class="form-control" placeholder="Konfirmasi Password">
                  </div>
                  <small>kosongkan jika tidak diubah</small>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label">Level <span class="tx-danger">*</span></label>
                  <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                    <select name="usertype" class="form-control select2-show-search" data-placeholder="Choose one (with searchbox)">
                      <option value="admin">Administrator</option>
                      <option value="author" <?php echo ($data->usertype == "author" ? "selected" : "") ?>>Author</option>
                    </select>
                  </div>
                </div>
                <div class="row mg-t-20">
                  <label class="col-sm-4 form-control-label"></label>
                  <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input type="submit" name="submit" value="Submit" class="btn btn-info">
                  </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
                </form>
              </div><!-- card -->
              <br>
            </div>
          </div><!-- row -->

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>
    </body>
    </html>
