<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{
		// START - define the variable for parsing to views
		$data['title'] = "Data Pengguna";
		$data['menu'] = "user";
		$data['submenu'] = "user";
		$data['action'] = base_url('user/add_process');

		//fetch list of data from table 'user' and 'dosen'
		$data['data'] = $this->crud->get_all('user', 'id, username, name, if(usertype = "admin", "Admin", "Author") as usertype', 'id ASC');
		$data['dosen'] = $this->crud->get_all('dosen', "kode as id, username, nama as name, 'Dosen' as usertype", 'nama ASC');
		// END - define the variable for parsing to views

		$this->load->view('list', $data);
	}

	public function delete($id = null)
	{
		//update status to '0'
		$this->crud->delete('user', array('id' => $id));

		//redirect to list page
		redirect(base_url('user'));
	}

	public function edit($id = null)
	{
		//$id shouldn't be null!!
		if($id == null)
		{
			redirect(base_url('user'));
		}

		// START - define the variable for parsing to views
		$data['title'] = "Ubah Data Pengguna";
		$data['menu'] = "user";
		$data['submenu'] = "user";
		$data['action'] = base_url('user/edit_process/'.$id);

		//fetch list of data from table 'user'
		$data['data'] = $this->crud->get_where('user', '*', array('id' => $id));

		// END - define the variable for parsing to views
		$this->load->view('form', $data);
	}

	public function edit_process($id = null)
	{
		//$id shouldn't be null
		if($id == null)
		{
			redirect(base_url('user'));
		}

		//get user input
		$input = $this->input->post(null, true);

		//set rules for validation
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('name', 'Nama', 'required');

		//check, is validation passed?
		if($this->form_validation->run() === TRUE)
		{

			// START - check username availability
			$where = array('username' => $input['username']);

			// fetch data
			$data = $this->crud->get_where('user', 'id', $where);

			// check is id same with input
			if(isset($data->id) && $data->id != $id)
			{
				//update procedure failure
				$response = array(
					'status' => 0,
					'message' => 'Username sudah digunakan.',
					'return_url' => '#'
				);

				die(json_encode($response));
			}

			// END - check username availability

			//set update field to table 'user'
			$update = array(
				'name' => $input['name'],
				'username' => $input['username'],
				'usertype' => $input['usertype']
			); 

			if($input['password'] != '')
			{
				$update['password'] = substr(md5($input['password']), 0, 15);
			}

			//set where clause
			$where = array(
				'id' => $id
			);

			//update table 'user'
			//check is update procedure ends up with success or not
			if($this->crud->update('user', $update, $where))
			{

				//set response status with success code (20)
				$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Pengguna',
					'return_url' => base_url('user')
				);

			} else {

				//update procedure failure
				$response = array(
					'status' => 0,
					'message' => 'Gagal Menyimpan Data',
					'return_url' => '#'
				);

			}
		} else { //if validation not passed
			
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		//print out response with json
		echo json_encode($response);
	}

	//method for add submission
	public function add_process()
	{
		//get user input from form
		$input = $this->input->post(null, true);

		//set rules for validation
		$this->form_validation->set_rules('username', 'Username', 'required|callback_username_check');
		$this->form_validation->set_rules('name', 'Nama', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('passconf', 'Password Konfirmasi', 'required|matches[password]');

		//check, is validation passed?
		if($this->form_validation->run() === TRUE)
		{
			//set insert field to table 'user'
			$insert = array(
				'username' => $input['username'],
				'name' => $input['name'],
				'password' => substr(md5($input['password']), 0, 15),
				'usertype' => $input['usertype']
			);

			//insert to table 'user'
			//check is insert procedure end up with success or not
			if($this->crud->insert('user', $insert)) 
			{
				
				//set response status with success code (20)
				$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Pengguna',
					'return_url' => base_url('user')
				);

			} else {

				//insert procedure failure
				$response = array(
					'status' => 0,
					'message' => 'Gagal Menyimpan Data',
					'return_url' => '#'
				);

			}
		} else { //if validation not passed
			
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		//print out response with json

		echo json_encode($response);
	}

	// method for callback check username
	public function username_check($str)
	{

		// set where clause
		$where = array('username' => $str);

		// fetch data
		$data = $this->crud->get_where('user', 'id', $where);

		// check data available or no
		if(!isset($data->id))
		{
			return true;
		} else {
			$this->form_validation->set_message('username_check', 'Username sudah digunakan, gunakan username lain!');
			return false;
		}
	}

}