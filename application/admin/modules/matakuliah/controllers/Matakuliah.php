<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Matakuliah extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{

		$data['title'] = "Mata Kuliah";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "matakuliah";
		$data['data'] = $this->crud->get_all('matakuliah', '*', 'nama ASC');

		$this->load->view('list', $data);
	}

	public function add()
	{
		$data['title'] = "Tambah Data Mata Kuliah";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "matakuliah";
		$data['action'] = base_url('matakuliah/add_process');
		// $data['dropdown_category'] = $this->dropdown_category();

		$this->load->view('form', $data);
	}

	public function edit($kode = null)
	{
		$data['data'] = $this->crud->get_where('matakuliah', '*', array('kode' => $kode));
		$data['title'] = "Ubah Data Mata Kuliah";
		$data['menu'] = "penjadwalan";
		$data['submenu'] = "matakuliah";
		$data['action'] = base_url('matakuliah/edit_process');

		$this->load->view('form', $data);
	}

	public function delete($kode = null)
	{
		$where = array(
			'kode' => $kode
		);
		$this->crud->delete('matakuliah', $where);

		redirect(base_url('matakuliah'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('kode_mk', 'Kode MK', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('sks', 'SKS', 'required|trim|numeric');
		$this->form_validation->set_rules('semester', 'Semester', 'required|trim|numeric');
		
		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['kode_mk'] = $this->security->xss_clean($input['kode_mk']);
			$input['nama'] = $this->security->xss_clean($input['nama']);
			$input['sks'] = $this->security->xss_clean($input['sks']);
			$input['semester'] = $this->security->xss_clean($input['semester']);

			$insert = array(
				'kode_mk' => $input['kode_mk'],
				'nama' => $input['nama'],
				'sks' => $input['sks'],
				'semester' => $input['semester'],
				'created_on' => date('Y-m-d H:i:s')
			);

			$this->crud->insert('matakuliah', $insert);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Matakuliah',
					'return_url' => base_url('matakuliah')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		$this->form_validation->set_rules('kode_mk', 'Kode MK', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('sks', 'SKS', 'required|trim|numeric');
		$this->form_validation->set_rules('semester', 'Semester', 'required|trim|numeric');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['kode_mk'] = $this->security->xss_clean($input['kode_mk']);
			$input['nama'] = $this->security->xss_clean($input['nama']);
			$input['sks'] = $this->security->xss_clean($input['sks']);
			$input['semester'] = $this->security->xss_clean($input['semester']);

			$update = array(
				'kode_mk' => $input['kode_mk'],
				'nama' => $input['nama'],
				'sks' => $input['sks'],
				'semester' => $input['semester'],
				'updated_on' => date('Y-m-d H:i:s')
			);

			$where = array(
				'kode' => $input['kode']
			);

			$this->crud->update('matakuliah', $update, $where);

			$response = array(
				'status' => 20,
				'message' => 'Berhasil Menyimpan Data Matakuliah',
				'return_url' => base_url('matakuliah')
			);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

}