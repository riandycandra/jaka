<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends MY_Controller {
	public function __construct()
	{
		$allowed_user = array('admin', 'author');
		if(!check_session($allowed_user, $this->session->userdata('usertype')))
		{
			redirect(base_url('dashboard'));
		}
		$this->load->model('M_pengumuman', 'pengumuman');
	}

	public function index()
	{
		// jika admin, tampilkan semua pengumuman, jika bukan, tampilkan pengumuman yang ditulis oleh user yg sedang login
		if($this->session->userdata('usertype') == 'admin')
		{
			$where = array(
				'status' => '2'
			);
		} else {
			$where = array(
				'user_id' => $this->session->userdata('id')
			);
		}


		$data['title'] = "Pengumuman";
		$data['menu'] = "pengumuman";
		$data['submenu'] = "list";

		$data['data_tampil'] = $this->pengumuman->get_all($where);
		$data['data'] = $this->pengumuman->get_all();

		if($this->session->userdata('usertype') == 'admin')
		{
			$this->load->view('list', $data);
		} else {
			$this->load->view('list_user', $data);
		}
	}

	// method untuk menyembunyikan pengumuman
	public function hide($id = null)
	{
		$where = array('id' => $id);
		$update = array('status' => '0');
		$this->crud->update('pengumuman', $update, $where);

		redirect(base_url('pengumuman'));
	}

	// method untuk menampilkan pengumuman yang tersembunyi
	public function show($id = null)
	{
		$where = array('id' => $id);
		$update = array('status' => '2');
		$this->crud->update('pengumuman', $update, $where);

		redirect(base_url('pengumuman'));
	}

	public function add()
	{
		$data['title'] = "Tambah Pengumuman";
		$data['menu'] = "pengumuman";
		$data['submenu'] = "form";
		$data['action'] = base_url('pengumuman/add_process');

		$this->load->view('form', $data);
	}

	public function edit($id = null)
	{
		$data['data'] = $this->crud->get_where('pengumuman', '*', array('id' => $id));
		$data['title'] = "Ubah Pengumuman";
		$data['menu'] = "pengumuman";
		$data['submenu'] = "form";
		$data['action'] = base_url('pengumuman/edit_process');

		$this->load->view('form', $data);
	}

	public function delete($id = null)
	{
		$where = array(
			'id' => $id
		);
		$this->crud->delete('pengumuman', $where);

		redirect(base_url('pengumuman'));
	}

	public function add_process()
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim');
		$this->form_validation->set_rules('expired_on', 'Tanggal Kadaluarsa', 'required|trim');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['title'] = $this->security->xss_clean($input['title']);
			$input['expired_on'] = $this->security->xss_clean($input['expired_on']);

			$insert = array(
				'title' => $input['title'],
				'expired_on' => $input['expired_on'],
				'content' => $this->input->post('content'),
				'user_id' => $this->session->userdata('id'),
				'created_on' => date('Y-m-d H:i:s')
			);

			$this->crud->insert('pengumuman', $insert);

			$response = array(
				'status' => 20,
				'message' => 'Berhasil Menyimpan Data Pengumuman',
				'return_url' => base_url('pengumuman')
			);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}

	public function edit_process()
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim');
		$this->form_validation->set_rules('expired_on', 'Tanggal Kadaluarsa', 'required|trim');
		$this->form_validation->set_rules('id', 'id', 'required|numeric|trim');

		if($this->form_validation->run() === TRUE)
		{
			$input = $this->input->post(null, true);

			$input['title'] = $this->security->xss_clean($input['title']);
			$input['expired_on'] = $this->security->xss_clean($input['expired_on']);
			$input['id'] = $this->security->xss_clean($input['id']);

			$update = array(
				'title' => $input['title'],
				'expired_on' => $input['expired_on'],
				'content' => $this->input->post('content'),
				// 'user_id' => $this->session->userdata('id'),
				'updated_on' => date('Y-m-d H:i:s')
			);

			$where = array(
				'id' => $input['id']
			);

			$this->crud->update('pengumuman', $update, $where);

			$response = array(
					'status' => 20,
					'message' => 'Berhasil Menyimpan Data Pengumuman',
					'return_url' => base_url('pengumuman')
				);

		} else {
			$response = array(
				'status' => 0,
				'message' => strip_tags(validation_errors()),
				'return_url' => '#'
			);

		}

		echo json_encode($response);
	}
}