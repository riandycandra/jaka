<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengumuman extends CI_Model {

	public function get_all($where = null)
	{
		$this->db->select('p.id, p.title, p.created_on, p.status, u.name, if(p.status = "2", "Tampil", "Belum Tampil") as status_tampil');
		$this->db->join('user u', 'u.id = p.user_id', 'INNER');
		if($where != null)
		{
			$this->db->where($where);
		}

		return $this->db->get('pengumuman p')->result();
	}

}