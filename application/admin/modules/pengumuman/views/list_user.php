<?php $i=1; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('template/head-tags') ?>
</head>

<body>
  <?php $this->load->view('template/sideleft') ?>
  <?php $this->load->view('template/head-panel') ?>
  <div class="br-mainpanel">
    <div class="br-pagebody mg-t-5 pd-x-30" style="margin-top: 100px;">
      <div class="row row-sm mg-t-20">
        <div class="col-12">
          <div class="card pd-0 bd-0 shadow-base mg-t-10">
            <div class="pd-x-30 pd-t-30 pd-b-15">
              <div class="d-flex align-items-center justify-content-between">
                <h3>Data Pengumuman</h3>
              </div>
              <!-- Editable table -->
              <div class="card">
                <div class="card-body">
                  <table class="table table-bordered table-responsive-md table-striped datatable text-center">
                    <thead>
                      <tr>
                        <th class="text-center">Penulis</th>
                        <th class="text-center">Judul</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">#</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data_tampil as $row) : ?>
                        <tr>
                          <td class="pt-3-half" ><?php echo $row->name ?></td>
                          <td class="pt-3-half" ><?php echo subword($row->title, 150) ?></td>
                          <td class="pt-3-half" ><?php echo ($row->status == '1' ? 'Pending' : ($row->status == '2' ? 'Disetujui' : 'Ditolak')) ?></td>
                          <td class="pt-3-half">
                            <a href="<?php echo base_url('pengumuman/edit/'.$row->id) ?>" class="btn btn-success btn-rounded btn-sm my-0"><i class="fa fa-pencil"></i></a>
                            <span><button data-toggle="confirm" data-text="Aksi ini tidak dapat dibatalkan" data-ico="warning" data-title="Apakah anda yakin ingin menghapus <?php echo $row->title ?>?" data-url="<?php echo base_url('pengumuman/delete/'.$row->id) ?>" type="button" class="btn btn-danger btn-rounded btn-sm my-0"><i class="fa fa-times"></i></button></span>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <!-- This is our clonable table line -->
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- Editable table -->
            </div>
          </div><!-- row -->

        </div><!-- br-pagebody -->
        <?php $this->load->view('template/footer-body') ?>
      </div><!-- br-mainpanel -->
      <!-- ########## END: MAIN PANEL ########## -->

      <?php $this->load->view('template/scripts') ?>
    </body>
    </html>
