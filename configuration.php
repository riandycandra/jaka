<?php

/*
 * DEFINE SOFTWARE STATUS
 * WARNING : DONT PUT 'development' IN PRODUCTION SERVER
 * IT'LL SHOW YOU ALL ERROR IN SOFTWARE (IF ANY)
 * PUT 'production' OR 'testing' ON PRODUCTION SERVER !!!
 */
define('ENVIRONMENT', 'development');

/*
 * DEFINE BASE URL
 */
define('BASE_URL', 'http://localhost/jaka/');

/*
 * DEFINE BASE URL FOR ADMIN
 */
define('BASE_URL_ADMIN', 'http://localhost/jaka/admin/');

/*
 * DEFINE DATABASE CONNECTION
 */
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'penjadwalan_genetik_web');

/*
 * DEFINE ROOT PATH
 */
define('ROOT_PATH', 'C:\xampp\htdocs\jaka');

/*
 * DEFINE TIME ZONE
 */
date_default_timezone_set('Asia/Jakarta');