-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: penjadwalan_genetik_web
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Kemahasiswaan');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'{\"site_name\":\"STMIK ROSMA\",\"phone\":\"0821-1020-5788\",\"email\":\"stmik@rosma.ac.id\",\"address\":\"Jl. Kertabumi No. 62 Karawang Barat 41311 Karawanga\",\"mahasiswa\":\"305\"}','2019-08-30 10:50:16','2019-08-30 10:54:21');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dosen`
--

DROP TABLE IF EXISTS `dosen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dosen` (
  `kode` int(2) NOT NULL AUTO_INCREMENT,
  `nidn` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(15) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dosen`
--

LOCK TABLES `dosen` WRITE;
/*!40000 ALTER TABLE `dosen` DISABLE KEYS */;
INSERT INTO `dosen` VALUES (1,'LLS','Lila Setiyani, M.Kom.',NULL,NULL,'lila','202cb962ac59075',NULL,NULL),(2,'RKM','H. Rukmanta Jayawiguna, Drs., MM',NULL,NULL,'rukmanta','202cb962ac59075',NULL,NULL),(3,'KRY','Karya Suhada, S.Kom, M.Kom',NULL,NULL,'karya','202cb962ac59075',NULL,NULL),(4,'DHN','Dhian Nur Rahayu, M.Kom',NULL,NULL,'dhian','202cb962ac59075',NULL,NULL),(9,'WKS','Widi Asih Kartika Sari, ST, M.Kom',NULL,NULL,'widi','202cb962ac59075',NULL,NULL),(10,'NSR','Nasri, S.Kom., S.Pd., M.T., M.Kom.',NULL,NULL,'nasri','202cb962ac59075',NULL,NULL),(11,'YDN','Yudiana, S.T.,M.Kom',NULL,NULL,'yudiana','202cb962ac59075',NULL,NULL),(12,'ARF','Arif Maulana Yusuf, S.Kom., M.Kom.',NULL,NULL,'arif','202cb962ac59075',NULL,NULL),(13,'ASB','Asep Samsul Bakhri, S.Kom, M.Kom.',NULL,NULL,'asep','202cb962ac59075',NULL,NULL),(15,'GIE','Anggi Elanda, S.Kom., M.kom.',NULL,NULL,'anggi','202cb962ac59075',NULL,NULL),(16,'ABD','Abdul Hamid, S.PdI., M.PdI.','karawang','123','abdul','202cb962ac59075',NULL,'2019-09-01 14:23:32'),(17,'DRM','Darmansyah, S.Kom, M.Kom',NULL,NULL,'darmansyah','202cb962ac59075',NULL,NULL),(19,'JJG','H. Jajang, S.Pd., M.Pd.',NULL,NULL,'jajang','202cb962ac59075',NULL,NULL),(21,'DNY','Donny Apdian, S.IP., M.M.',NULL,NULL,'donny','202cb962ac59075',NULL,NULL),(22,'YYS','Yahya Suherman, S.S., M.M.',NULL,NULL,'yahya','202cb962ac59075',NULL,NULL),(23,'MEL','Rini Malfiany, S.Pd, S.Kom., M.Kom',NULL,NULL,'rini','202cb962ac59075',NULL,NULL),(24,'RMG','R. Mega Yulianto, S.S.T., M.Pd.',NULL,NULL,'mega','202cb962ac59075',NULL,NULL),(25,'DHM','Dhima Rosihan Muhtar, S.Kom.,MOS\r\n',NULL,NULL,'dhima','202cb962ac59075',NULL,NULL);
/*!40000 ALTER TABLE `dosen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `content` text,
  `date` date DEFAULT NULL,
  `place` text,
  `banner` varchar(20) DEFAULT NULL,
  `link` text,
  `user_id` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `event_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'Lorem Ipsum','Lorem Ipsum','2019-08-31','Hotel Mercure','123.jpg','https://www.google.com/',2,'2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Seminar e-Commerce','<p>awdawdawd awd awd awd awd awdd a</p>\r\n','2019-09-04','Hotel Swiss Bellin','be836242e743232.jpg','https://www.google.com/',2,'2','2019-08-29 21:47:27','2019-09-09 07:52:59'),(5,'Seminar IT Business','<p>awdawdawd aw daw dawd</p>\r\n','2019-09-01','Tes','e2d1f4638b108a5.jpg','https://www.google.com/',2,'2','2019-08-29 21:48:35','2019-09-09 07:53:30'),(6,'tes brian','<p>awdawd</p>\r\n','2019-08-31','awdawdawd','4dfb28667af7d2f.jpg','https://www.google.com/',2,'2','2019-08-29 21:50:21','0000-00-00 00:00:00'),(7,'admin tes','<p>awdawdawdawd</p>\r\n','2019-08-16','Tes','f12054eb66af8c5.jpg','https://www.google.com/',1,'2','2019-08-29 21:50:50','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hari`
--

DROP TABLE IF EXISTS `hari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hari` (
  `kode` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hari`
--

LOCK TABLES `hari` WRITE;
/*!40000 ALTER TABLE `hari` DISABLE KEYS */;
INSERT INTO `hari` VALUES (1,'Senin',NULL,NULL),(2,'Selasa',NULL,NULL),(3,'Rabu',NULL,NULL),(4,'Kamis',NULL,NULL),(5,'Jumat',NULL,NULL),(6,'Sabtu',NULL,NULL);
/*!40000 ALTER TABLE `hari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jadwalkuliah`
--

DROP TABLE IF EXISTS `jadwalkuliah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jadwalkuliah` (
  `kode` int(10) NOT NULL AUTO_INCREMENT,
  `kode_pengampu` int(10) DEFAULT NULL,
  `kode_jam` int(10) DEFAULT NULL,
  `kode_hari` int(10) DEFAULT NULL,
  `kode_ruang` int(10) DEFAULT NULL,
  PRIMARY KEY (`kode`),
  KEY `kode_pengampu` (`kode_pengampu`),
  KEY `kode_jam` (`kode_jam`),
  KEY `kode_hari` (`kode_hari`),
  KEY `kode_ruang` (`kode_ruang`),
  CONSTRAINT `jadwalkuliah_ibfk_1` FOREIGN KEY (`kode_pengampu`) REFERENCES `pengampu` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jadwalkuliah_ibfk_2` FOREIGN KEY (`kode_jam`) REFERENCES `jam` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jadwalkuliah_ibfk_3` FOREIGN KEY (`kode_hari`) REFERENCES `hari` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jadwalkuliah_ibfk_4` FOREIGN KEY (`kode_ruang`) REFERENCES `ruang` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1 COMMENT='hasil proses';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jadwalkuliah`
--

LOCK TABLES `jadwalkuliah` WRITE;
/*!40000 ALTER TABLE `jadwalkuliah` DISABLE KEYS */;
INSERT INTO `jadwalkuliah` VALUES (1,1,10,1,1),(2,2,12,5,6),(3,3,8,1,8),(4,4,12,4,9),(5,5,10,1,11),(6,6,10,3,4),(7,7,10,6,2),(8,8,8,5,1),(9,9,13,6,6),(10,10,15,6,14),(11,11,12,5,9),(12,12,15,2,2),(13,13,14,2,14),(14,14,9,2,10),(15,15,14,4,1),(16,16,17,6,10),(17,17,16,3,5),(18,18,13,6,4),(19,19,13,6,12),(20,20,8,1,7),(21,21,14,1,4),(22,22,12,2,12),(23,23,15,4,9),(24,24,8,5,5),(25,1,2,5,9),(26,2,3,2,10),(27,3,1,4,12),(28,4,2,6,9),(29,5,1,6,6),(30,6,6,3,3),(31,7,2,4,5),(32,8,3,1,7),(33,9,1,5,6),(34,10,1,5,4),(35,11,3,5,11),(36,12,4,4,4),(37,13,3,4,12),(38,14,4,1,5),(39,15,1,2,2),(40,16,1,5,8),(41,17,1,3,10),(42,18,1,3,13),(43,19,5,6,13),(44,20,5,6,14),(45,21,5,2,11),(46,22,1,3,6),(47,23,5,1,13),(48,24,5,2,5);
/*!40000 ALTER TABLE `jadwalkuliah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jam`
--

DROP TABLE IF EXISTS `jam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jam` (
  `kode` int(10) NOT NULL AUTO_INCREMENT,
  `range_jam` varchar(50) DEFAULT NULL,
  `aktif` enum('Y','N') DEFAULT NULL,
  `tipe` enum('pagi','sore') DEFAULT 'pagi',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jam`
--

LOCK TABLES `jam` WRITE;
/*!40000 ALTER TABLE `jam` DISABLE KEYS */;
INSERT INTO `jam` VALUES (1,'18.00-18.35',NULL,'sore',NULL,NULL),(2,'18.35-19.10',NULL,'sore',NULL,NULL),(3,'19.10-19.45',NULL,'sore',NULL,NULL),(4,'19.45-20.20',NULL,'sore',NULL,NULL),(5,'20.20-20.55',NULL,'sore',NULL,NULL),(6,'20.55-21.30',NULL,'sore',NULL,NULL),(7,'21.30-22.05',NULL,'sore',NULL,NULL),(8,'08.30-09.20',NULL,'pagi',NULL,NULL),(9,'09.20-10.10',NULL,'pagi',NULL,NULL),(10,'10.10-11.00',NULL,'pagi',NULL,NULL),(11,'11.00-11.50',NULL,'pagi',NULL,NULL),(12,'11.50-12.40',NULL,'pagi',NULL,NULL),(13,'12.40-13.30',NULL,'pagi',NULL,NULL),(14,'13.30-14.20',NULL,'pagi',NULL,NULL),(15,'14.20-15.10',NULL,'pagi',NULL,NULL),(16,'15.10-16.00',NULL,'pagi',NULL,NULL),(17,'16.00-16.50',NULL,'pagi',NULL,NULL),(18,'16.50-17.40',NULL,'pagi',NULL,NULL);
/*!40000 ALTER TABLE `jam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matakuliah`
--

DROP TABLE IF EXISTS `matakuliah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matakuliah` (
  `kode` int(10) NOT NULL AUTO_INCREMENT,
  `kode_mk` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `sks` int(6) DEFAULT NULL,
  `semester` int(6) DEFAULT NULL,
  `aktif` enum('True','False') DEFAULT 'True',
  `jenis` enum('TEORI','PRAKTIKUM') DEFAULT 'TEORI',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1 COMMENT='example kode_mk = 0765109 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matakuliah`
--

LOCK TABLES `matakuliah` WRITE;
/*!40000 ALTER TABLE `matakuliah` DISABLE KEYS */;
INSERT INTO `matakuliah` VALUES (1,NULL,'Testing Implementasi  Software',3,8,'True','TEORI',NULL,NULL),(2,NULL,'Kepemimpinan',2,8,'True','TEORI',NULL,NULL),(3,NULL,'Skripsi',2,8,'True','TEORI',NULL,NULL),(4,NULL,'Pemrograman  Web',3,6,'True','TEORI',NULL,NULL),(5,NULL,'Manajemen Proyek',3,6,'True','TEORI',NULL,NULL),(6,NULL,'Metode Penelitian',2,6,'True','TEORI',NULL,NULL),(7,NULL,'Bahasa Indonesia',2,6,'True','TEORI',NULL,NULL),(8,NULL,'Pemrograman  Visual (.Net)',4,6,'True','TEORI',NULL,NULL),(9,NULL,'Teknik Kompilasi dan Otomata',4,6,'True','TEORI',NULL,NULL),(10,NULL,'Sistem Multimedia',3,6,'True','TEORI',NULL,NULL),(11,NULL,'Metode Numerik',2,4,'True','TEORI',NULL,NULL),(12,NULL,'Pemrograman  Visual 1',4,4,'True','TEORI',NULL,NULL),(13,NULL,'Interaksi Manusia & Komputer',2,4,'True','TEORI',NULL,NULL),(14,NULL,'Analisa dan Perancangan Software',4,4,'True','TEORI',NULL,NULL),(15,NULL,'Manajemen Jaringan',4,4,'True','TEORI',NULL,NULL),(16,'AGM','Agama',2,4,'True','TEORI',NULL,'2019-09-01 17:57:17'),(17,NULL,'Perancangan Basis Data',3,4,'True','TEORI',NULL,NULL),(18,NULL,'Struktur Data',4,2,'True','TEORI',NULL,NULL),(19,NULL,'Aljabar Linear dan Matriks',3,2,'True','TEORI',NULL,NULL),(20,NULL,'Arsitektur dan Organisasi Komputer',3,2,'True','TEORI',NULL,NULL),(21,NULL,'Kewarganegaraan',2,2,'True','TEORI',NULL,NULL),(22,NULL,'B. Inggris Informatika 2',2,2,'True','TEORI',NULL,NULL),(23,NULL,'Statistik',3,2,'True','TEORI',NULL,NULL),(24,NULL,'Logika Informatika',3,2,'True','TEORI',NULL,NULL);
/*!40000 ALTER TABLE `matakuliah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  `link` text NOT NULL,
  `order` int(2) NOT NULL,
  `type` enum('internal','eksternal') NOT NULL DEFAULT 'eksternal',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Pengumuman','http://localhost/jaka/pengumuman',4,'internal'),(2,'Berita','http://localhost/jaka/berita',1,'internal'),(3,'Event','http://localhost/jaka/event',2,'internal'),(4,'Portal Mahasiswa','https://www.google.com/',3,'eksternal'),(5,'Portal Dosen','https://www.google.com/',0,'eksternal'),(6,'E-Library','https://www.google.com/',5,'eksternal'),(7,'Jadwal Kuliah','http://localhost/jaka/jadwal',6,'internal'),(8,'Jurnal Interkom','https://www.google.com/',7,'eksternal'),(9,'Ringkasan Materi','http://localhost/jaka/resume',8,'internal');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `content` text,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0 = ditolak, 1 = pending, 2 = disetujui',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `news_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `news_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (3,'Penyuluhan Narkoba','<p>STMIK ROSMA, menjadi kampus pertama yang dilakukan penyuluhan oleh BNN Kabupaten Karawang, peserta pada penyuluhan ini diantaranya, mahasiswa, dan para staff dosen STMIK ROSMA. Diharapkan dengan adanya penyuluhan ini, STMIK ROSMA terhindar dari efek narkoba.</p>\r\n',1,1,'2','2019-08-27 21:14:28','2019-09-09 07:50:39'),(7,'Lorem ipsum dolor sit ameta','<p>Lorem ipsum dolor sit amet</p>\r\n',1,2,'2','2019-08-29 15:07:43','2019-08-29 15:15:07'),(8,'Lorem ipsum dolor sit amet, consectetur adipisicing elit.','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias laudantium nemo, eveniet excepturi suscipit amet, illo autem quasi sequi illum quisquam doloremque est magni accusamus quo officia, voluptates temporibus cum modi, reiciendis beatae deleniti. Itaque iure porro accusantium expedita sequi qui harum consequatur incidunt minima ratione eum velit enim, voluptatem repudiandae quam. Deserunt libero praesentium facilis quos iste officiis dignissimos magni, repellat modi vero eius eos eum id tempora placeat consectetur quisquam velit ad. Laudantium quisquam rem repudiandae eum, neque laborum alias blanditiis illo ad tenetur, cum sint, quia placeat sunt quo consequuntur optio iusto similique dolores vel veniam distinctio?</p>\r\n',1,1,'2','2019-09-08 20:10:09','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengampu`
--

DROP TABLE IF EXISTS `pengampu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengampu` (
  `kode` int(10) NOT NULL AUTO_INCREMENT,
  `kode_mk` int(10) DEFAULT NULL,
  `kode_dosen` int(10) DEFAULT NULL,
  `kelas` varchar(10) DEFAULT NULL,
  `tahun_akademik` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`kode`),
  KEY `kode_mk` (`kode_mk`),
  KEY `kode_dosen` (`kode_dosen`),
  CONSTRAINT `pengampu_ibfk_1` FOREIGN KEY (`kode_mk`) REFERENCES `matakuliah` (`kode`),
  CONSTRAINT `pengampu_ibfk_2` FOREIGN KEY (`kode_dosen`) REFERENCES `dosen` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengampu`
--

LOCK TABLES `pengampu` WRITE;
/*!40000 ALTER TABLE `pengampu` DISABLE KEYS */;
INSERT INTO `pengampu` VALUES (1,1,1,NULL,'2018-2019'),(2,2,2,NULL,'2018-2019'),(3,3,3,NULL,'2018-2019'),(4,4,4,NULL,'2018-2019'),(5,5,3,NULL,'2018-2019'),(6,6,1,NULL,'2018-2019'),(7,7,2,NULL,'2018-2019'),(8,8,25,NULL,'2018-2019'),(9,9,9,NULL,'2018-2019'),(10,10,10,NULL,'2018-2019'),(11,11,11,NULL,'2018-2019'),(12,12,12,NULL,'2018-2019'),(13,13,13,NULL,'2018-2019'),(14,14,4,NULL,'2018-2019'),(15,15,15,NULL,'2018-2019'),(16,16,16,NULL,'2018-2019'),(17,17,17,NULL,'2018-2019'),(18,18,3,NULL,'2018-2019'),(19,19,19,NULL,'2018-2019'),(20,20,13,NULL,'2018-2019'),(21,21,21,NULL,'2018-2019'),(22,22,22,NULL,'2018-2019'),(23,23,23,NULL,'2018-2019'),(24,24,24,NULL,'2018-2019');
/*!40000 ALTER TABLE `pengampu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengumuman`
--

DROP TABLE IF EXISTS `pengumuman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengumuman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `content` text,
  `user_id` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0 = ditolak, 1 = pending, 2 = disetujui',
  `show` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 = hide, 1 = show',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengumuman`
--

LOCK TABLES `pengumuman` WRITE;
/*!40000 ALTER TABLE `pengumuman` DISABLE KEYS */;
INSERT INTO `pengumuman` VALUES (1,'Tes Pengumuman','<p>Riandy Cnadra winahyu</p>\r\n',1,'0','0','2019-09-01 07:19:26','0000-00-00 00:00:00'),(2,'Tes pengumuman user','<p>awidjawidjuiawjdj</p>\r\n',2,'2','0','2019-09-01 07:22:38','0000-00-00 00:00:00'),(3,'Batas Pembayaran SPP','<p>Untuk mahasiswa stmik rosma, yang belum melunasi SPP thn akademik 2019/2020 harap segera untuk melunasi sebelum tanggal 20 Juni 2019.</p>\r\n',5,'2','0','2019-09-08 20:26:38','2019-09-09 07:49:34'),(4,'Batas Pengajuan FRSS','<p>Pengisian FRSS Semester Genap dibatasi sampai tanggal 29 Juni 2019, mohon agar disegerakan dalam pengisian, karena harus ada tinjauan dari kaprodi.</p>\r\n',3,'2','0','2019-09-08 20:26:38','2019-09-09 07:47:39'),(5,'LPPM Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus reiciendis et sit, enim harum ullam.','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos dolore excepturi dolor possimus doloremque alias debitis provident temporibus quisquam? Cum ex maxime, unde, quia ullam nam dignissimos eum magni similique amet eos quos. Consequuntur dolorum quidem rerum aspernatur, quas sint aliquid incidunt non! Impedit vero officia commodi molestias ipsa quibusdam!</p>\r\n',4,'2','0','2019-09-08 20:26:38','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `pengumuman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resume`
--

DROP TABLE IF EXISTS `resume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertemuan_no` tinyint(4) NOT NULL,
  `pengampu_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` text,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pengampu_id` (`pengampu_id`),
  KEY `resume_ibfk_2` (`user_id`),
  CONSTRAINT `resume_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `dosen` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `resume_ibfk_3` FOREIGN KEY (`pengampu_id`) REFERENCES `pengampu` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resume`
--

LOCK TABLES `resume` WRITE;
/*!40000 ALTER TABLE `resume` DISABLE KEYS */;
INSERT INTO `resume` VALUES (1,1,1,1,'<p>sadawdawdaw dawdawd awdawd awd</p>','2019-09-05 13:23:29','2019-09-05 14:22:21'),(2,1,6,1,'<p>tes pertemuan ke 1</p>','2019-09-05 14:27:59','0000-00-00 00:00:00'),(3,2,1,1,'<p>testsetsetestse tset se tse test</p>','2019-09-05 14:28:44','2019-09-05 14:28:44'),(6,1,19,19,'<p>Pengenalan aljabar</p>','2019-09-06 08:58:11','2019-09-06 08:58:11');
/*!40000 ALTER TABLE `resume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ruang`
--

DROP TABLE IF EXISTS `ruang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ruang` (
  `kode` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `kapasitas` int(10) DEFAULT NULL,
  `jenis` enum('TEORI','LABORATORIUM') DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ruang`
--

LOCK TABLES `ruang` WRITE;
/*!40000 ALTER TABLE `ruang` DISABLE KEYS */;
INSERT INTO `ruang` VALUES (1,'A101',NULL,'TEORI',NULL,NULL),(2,'A301',NULL,'TEORI',NULL,NULL),(3,'D201',NULL,'TEORI',NULL,NULL),(4,'E203',NULL,'TEORI',NULL,NULL),(5,'AULA SG',NULL,'TEORI',NULL,NULL),(6,'LAB 201',NULL,'TEORI',NULL,NULL),(7,'A102',NULL,'TEORI',NULL,NULL),(8,'LAB 202',NULL,'TEORI',NULL,NULL),(9,'A302',NULL,'TEORI',NULL,NULL),(10,'D101',NULL,'TEORI',NULL,NULL),(11,'D105',NULL,'TEORI',NULL,NULL),(12,'D103',NULL,'TEORI',NULL,NULL),(13,'D104',NULL,'TEORI',NULL,NULL),(14,'D102',NULL,'TEORI',NULL,NULL);
/*!40000 ALTER TABLE `ruang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(15) NOT NULL,
  `usertype` enum('admin','author','dosen') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin','202cb962ac59075','admin'),(2,'Akademik','akademik','202cb962ac59075','author'),(3,'Prodi','prodi','202cb962ac59075','author'),(4,'Lppm','lppm','0c42b7f0cc6a095','author'),(5,'Keuangan','keuangan','202cb962ac59075','author');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waktu_tidak_bersedia`
--

DROP TABLE IF EXISTS `waktu_tidak_bersedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waktu_tidak_bersedia` (
  `kode` int(10) NOT NULL AUTO_INCREMENT,
  `kode_dosen` int(10) DEFAULT NULL,
  `kode_hari` int(10) DEFAULT NULL,
  `kode_jam` int(10) DEFAULT NULL,
  PRIMARY KEY (`kode`),
  KEY `kode_dosen` (`kode_dosen`),
  KEY `kode_hari` (`kode_hari`),
  KEY `kode_jam` (`kode_jam`),
  CONSTRAINT `waktu_tidak_bersedia_ibfk_1` FOREIGN KEY (`kode_dosen`) REFERENCES `dosen` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `waktu_tidak_bersedia_ibfk_2` FOREIGN KEY (`kode_hari`) REFERENCES `hari` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `waktu_tidak_bersedia_ibfk_3` FOREIGN KEY (`kode_jam`) REFERENCES `jam` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waktu_tidak_bersedia`
--

LOCK TABLES `waktu_tidak_bersedia` WRITE;
/*!40000 ALTER TABLE `waktu_tidak_bersedia` DISABLE KEYS */;
INSERT INTO `waktu_tidak_bersedia` VALUES (1,16,1,8),(2,16,1,7);
/*!40000 ALTER TABLE `waktu_tidak_bersedia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-09  7:56:18
